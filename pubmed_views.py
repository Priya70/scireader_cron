# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.timezone import utc
from django.utils.encoding import smart_str, smart_unicode
from django.db.models import Q, Sum, Count
from scireader.settings import ADMINS
from common.common import writeIntoDB, getFieldName, getCSVdata, run_mysql_script, id2eid, pt, rm, writeIntoDBBulk,writeIntoDBSingleInsert
from pubmed.models import *
from pubmed.forms import PMIDForm
from backend.views import write_cron_job_log
from backend.models import CronJobLog
from twitter.views import twitter_expanded_url
from twitter.models import ShortPattern
from Bio import Entrez
from Bio import Medline
from dateutil.relativedelta import relativedelta
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from time import strftime
import os, time, glob, json, re, types, urllib2, datetime, time, boto

Entrez.email = ADMINS[0][1]
Entrez.tool = "SciReader"
s3 = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY,host="s3-us-west-1.amazonaws.com")
bucket = s3.get_bucket('scireader-data',validate=False)

# In order not to overload the E-utility servers, NCBI recommends that users post no more than three URL requests per second and limit large jobs to either weekends or between 9:00 PM and 5:00 AM Eastern time during weekdays.

################################################################################
# Use Entrez

def getPubmedTables():
    # provide all pubmed tables
    pt("Return a list of tables...")
    dblist = {}
    dblist["Pubmed"] = Pubmed
    dblist["Abstract"] = Abstract
    dblist["Author"] = Author
    dblist["DataBank"] = DataBank
    dblist["Grant"] = Grant
    dblist["PublicationType"] = PublicationType
    dblist["ChemicalList"] = ChemicalList
    dblist["SupplMeshList"] = SupplMeshList
    dblist["CommentsCorrections"] = CommentsCorrections
    dblist["GeneSymbolList"] = GeneSymbolList
    dblist["MeshHeading"] = MeshHeading
    dblist["Keyword"] = Keyword
    return (dblist)

def getPubmedDictKeyDir(group):
    # return key directory for pubmed database
    keydir={}
    if group == "pubmed":
        keydir["Owner"] = "..Owner"
        keydir["Status"] = "..Status"
        keydir["PMIDVersion"] = "PMID..Version"
        keydir["DateCreated"] = "DateCreated..date"
        keydir["DateCompleted"] = "DateCompleted..date"
        keydir["DateRevised"] = "DateRevised..date"
        keydir["PubModel"] = "Article..PubModel"
        keydir["ISSN"] = "Article_Journal_ISSN"
        keydir["IssnType"] = "Article_Journal_ISSN..IssnType"
        keydir["CitedMedium"] = "Article_Journal_JournalIssue..CitedMedium"
        keydir["Volume"] = "Article_Journal_JournalIssue_Volume"
        keydir["Issue"] = "Article_Journal_JournalIssue_Issue"
        keydir["PubDate"] = "Article_Journal_JournalIssue_PubDate..date"
        keydir["JournalTitle"] = "Article_Journal_Title"
        keydir["ISOAbbreviation"] = "Article_Journal_ISOAbbreviation"
        keydir["ArticleTitle"] = "Article_ArticleTitle"
        keydir["Pagination"] = "Article_Pagination_MedlinePgn"
        keydir["DOI"] = "Article_ELocationID..EIdType..doi"
        keydir["PII"] = "Article_ELocationID..EIdType..pii"
        keydir["Affiliation"] = "Article_Affiliation"
        keydir["Language"] = "Article_Language"
        keydir["VernacularTitle"] = "Article_VernacularTitle"
        keydir["ArticleDate"] = "Article_ArticleDate..date"
        keydir["Country"] = "MedlineJournalInfo_Country"
        keydir["MedlineTA"] = "MedlineJournalInfo_MedlineTA"
        keydir["NlmUniqueID"] = "MedlineJournalInfo_NlmUniqueID"
        keydir["ISSNLinking"] = "MedlineJournalInfo_ISSNLinking"
        keydir["CitationSubset"] = "CitationSubset"
        keydir["NumberOfReferences"] = "NumberOfReferences"
        keydir["OtherID"] = "OtherID"
        keydir["OtherIDSource"] = "OtherID..Source"
    elif group == "pubmeddata":
        keydir["entrez"] = "History..PubStatus..entrez"
        keydir["pubmed"] = "History..PubStatus..pubmed"
        keydir["medline"] = "History..PubStatus..medline"
        keydir["DOI"] = "ArticleIdList..IdType..doi"
    elif group == "abstract":
        keydir["AbstractText"] = ""
        keydir["Label"] = "..Label"
        keydir["NlmCategory"] = "..NlmCategory"
    elif group == "author":
        keydir["LastName"] = "LastName"
        keydir["ForeName"] = "ForeName"
        keydir["Suffix"] = "Suffix"
        keydir["Initials"] = "Initials"
        keydir["Identifier"] = "Identifier"
        keydir["CollectiveName"] = "CollectiveName"
    elif group == "databank":
        keydir["AccessionNumber"] = ""
    elif group == "grant":
        keydir["GrantID"] = "Grant_GrantID"
        keydir["Acronym"] = "Grant_Acronym"
        keydir["Agency"] = "Grant_Agency"
        keydir["Country"] = "Grant_Country"
    elif group == "publicationtype":
        keydir["PublicationType"] = ""
    elif group == "chemical":
        keydir["RegistryNumber"] = "RegistryNumber"
        keydir["NameOfSubstance"] = "NameOfSubstance"
    elif group == "mesh":
        keydir["SupplMeshName"] = ""
        keydir["Type"] = ".Type"
    elif group == "comments":
        keydir["RefType"] = ".RefType"
        keydir["RefSource"] = "RefSource"
        keydir["RefPMID"] = "PMID"
        keydir["Note"] = "Note"
    elif group == "gene":
        keydir["GeneSymbol"] = ""
    elif group == "meshheading":
        keydir["DescriptorName"] = "DescriptorName"
        keydir["Type"] = "DescriptorName..Type"
        keydir["QualifierName"] = "QualifierName"
    elif group == "keyword":
        keydir["Keyword"] = ""
        keydir["Owner"] = "..Owner"
        
    return (keydir)

def PubmedDict2TableDict(ob,keydir):
    # the script will extract data for given ob and keydir
    data={}
    debug=False
    for key in keydir:
        if debug:
            print "----------"
            print "Key: %s" % key
        data[key] = ""
        path = keydir[key].split("_")
        if debug:
            print "Path: %s" % path
        dataobject=ob
        found=1 # to check if the key exists
        for p in path:
            if p != "": # avoid the empty data
                if str(type(dataobject)).find('StringElement') == -1:
                    if p in dataobject:
                        dataobject=dataobject[p]
                    else:
                        found=0
                else:
                    found=0
        if debug:
            print "Found: %s (1 for yes, 0 for no)" % found
        if found == 1:
            if str(type(dataobject)).find('list') != -1: # check if array
                data[key] = str(smart_str(' '.join(dataobject)))
            else:
                data[key] = str(smart_str(''.join(dataobject)))
        else:
            p=path.pop()
            if p.find('..') != -1:
                atti=p.split('..')
                if debug:
                    print "Attributions: %s" % atti
                if atti[0] != "":
                    if atti[0] in dataobject: # check if the key exists
                        dataobject=dataobject[atti[0]]
                    else: # provide empty value if not
                        dataobject=[]
                if debug:
                    print "Data object: %s" % dataobject
                if dataobject != []:
                    if atti[1] == "date": # extract the date information
                        if str(type(dataobject)).find('list') != -1:
                            dataobject = dataobject[0]
                        data[key]=extractPubmedDate(dataobject)
                    else: # extract the attribute information
                        if len(atti) > 2: # value equal to some particular attribute
                            for do in dataobject:
                                if atti[2] == do.attributes[atti[1]]:
                                    if atti[2] == "medline" or atti[2] == "entrez" or atti[2] == "pubmed":
                                        data[key]=extractPubmedDate(do)
                                    else:
                                        data[key]=str(do.encode('ascii','ignore'))
                        else:
                            if str(type(dataobject)).find('list') != -1: # check if array
                                dataobject=dataobject[0]
                            if str(type(dataobject)).find('StringElement') == -1 and str(type(dataobject)).find('UnicodeElement') == -1: # not a string element and not a unicode element
                                if atti[1] in dataobject.attributes: # make sure the atti exists
                                    data[key]=str(dataobject.attributes[atti[1]])
        if debug:
            print "Result: %s" % data[key]
        #data[key] = data[key].encode('ascii','ignore')
        if data[key] == "":
            data.pop(key,None)
    for key in data:
        data[key] = re.sub("\n", " ", data[key])
        data[key] = re.sub("  ", " ", data[key])
    return data

def pmid2DBdata(pmidarray):
    # input as pmidarray
    # out put as array of dictionary of data for each table
    dbdataarray = []
    pmids = ""
    for pmid in pmidarray:
        pmids = pmids + str(pmid) + ","
    data = Entrez.read(Entrez.efetch(db="pubmed", id=pmids,retmode='xml'))
    for pmiddata in data:
        if 'MedlineCitation' in pmiddata:
            pmid = int(pmiddata["MedlineCitation"]["PMID"])
            dbdata = {}
            dbdata["pmid"] = pmid
            #print "Parsing for %s..." % pmid
            # status
            if Pubmed.objects.filter(id=pmid).count() > 0:
                dbdata["indb"] = True
                #pt("Replacing %s..." % pmid)
                #print '>',
            else:
                dbdata["indb"] = False
                #pt("Creating %s..." % pmid)
                #print '.',
            dbdata["table"] = {}
            # pubmed
            dbdata["table"]["Pubmed"] = {}
            dbdata["table"]["Pubmed"] = PubmedDict2TableDict(pmiddata['MedlineCitation'], getPubmedDictKeyDir("pubmed"))
            dbdata["table"]["Pubmed"]["id"] = int(pmid)
            dbdata["table"]["Pubmed"]["PMID"] = pmid
            if "ArticleTitle" in dbdata["table"]["Pubmed"]:
                dbdata["table"]["Pubmed"]["ArticleTitle"] = dbdata["table"]["Pubmed"]["ArticleTitle"].replace('\xf0\x9d\x94\xbd','F')
            # get the entrez date
            d = {}
            d=PubmedDict2TableDict(pmiddata['PubmedData'], getPubmedDictKeyDir("pubmeddata"))
            if "entrez" in d:
                dbdata["table"]["Pubmed"]["DateCreated"] = d["entrez"]
                if not "DOI" in dbdata["table"]["Pubmed"]:
                    if "DOI" in d:
                        dbdata["table"]["Pubmed"]["DOI"] = d["DOI"]
            dbdata["table"]["Pubmed"]["DateToScireader"] = datetime.datetime.utcnow().replace(tzinfo=utc)
            
            if dbdata["indb"] == False:
                # abstract
                if "Abstract" in pmiddata['MedlineCitation']['Article']:
                    dbdata["table"]["Abstract"] = []
                    datalist=pmiddata['MedlineCitation']['Article']['Abstract']['AbstractText']
                    i = 1
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("abstract"))
                        d["PMID_id"]=int(pmid)
                        d["AbstractOrder"] = i
                        if "CopyrightInformation" in pmiddata['MedlineCitation']['Article']['Abstract']:
                            d["CopyrightInformation"] = str(smart_str(pmiddata['MedlineCitation']['Article']['Abstract']["CopyrightInformation"]))
                        dbdata["table"]["Abstract"].append(d)
                        i = i + 1
                # Author
                if "AuthorList" in pmiddata['MedlineCitation']['Article']:
                    dbdata["table"]["Author"] = []
                    datalist=pmiddata['MedlineCitation']['Article']['AuthorList']
                    i = 1
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("author"))
                        d["PMID_id"]=int(pmid)
                        d["AuthorOrder"] = i
                        dbdata["table"]["Author"].append(d)
                        i = i + 1
                # DataBank
                if "DataBankList" in pmiddata['MedlineCitation']['Article']:
                    dbdata["table"]["DataBank"] = []
                    datalistarray=pmiddata['MedlineCitation']['Article']['DataBankList']
                    #assert False, datalistarray
                    for k in datalistarray:
                        datalist=k["AccessionNumberList"]
                        for j in datalist:
                            d={}
                            d=PubmedDict2TableDict(j, getPubmedDictKeyDir("databank"))
                            d["PMID_id"]=int(pmid)
                            d["DataBankName"]=str(smart_str(k['DataBankName']))
                            dbdata["table"]["DataBank"].append(d)
                # Grant
                if "GrantList" in pmiddata['MedlineCitation']['Article']:
                    dbdata["table"]["Grant"] = []
                    datalist=pmiddata['MedlineCitation']['Article']['GrantList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("grant"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["Grant"].append(d)
                # PublicationType
                if "PublicationTypeList" in pmiddata['MedlineCitation']['Article']:
                    dbdata["table"]["PublicationType"] = []
                    datalist=pmiddata['MedlineCitation']['Article']['PublicationTypeList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("publicationtype"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["PublicationType"].append(d)
                # ChemicalList
                if "ChemicalList" in pmiddata['MedlineCitation']:
                    dbdata["table"]["ChemicalList"] = []
                    datalist=pmiddata['MedlineCitation']['ChemicalList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("chemical"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["ChemicalList"].append(d)
                # SupplMeshList
                if "SupplMeshList" in pmiddata['MedlineCitation']:
                    dbdata["table"]["SupplMeshList"] = []
                    if str(pmiddata['MedlineCitation']['SupplMeshList']).find('StringElement') == -1:
                        datalist=pmiddata['MedlineCitation']['SupplMeshList']['SupplMeshName']
                        for j in datalist:
                            d={}
                            d=PubmedDict2TableDict(j, getPubmedDictKeyDir("mesh"))
                            d["PMID_id"]=int(pmid)
                            dbdata["table"]["SupplMeshList"].append(d)
                    else:
                        d={}
                        d["SupplMeshName"]=str(pmiddata['MedlineCitation']['SupplMeshList'][0])
                        d["Type"]=pmiddata['MedlineCitation']['SupplMeshList'][0].attributes["Type"]
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["SupplMeshList"].append(d)
                # CommentsCorrections
                if "CommentsCorrectionsList" in pmiddata['MedlineCitation']:
                    dbdata["table"]["CommentsCorrections"] = []
                    datalist=pmiddata['MedlineCitation']['CommentsCorrectionsList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("comments"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["CommentsCorrections"].append(d)
                # GeneSymbolList
                if "GeneSymbolList" in pmiddata['MedlineCitation']:
                    dbdata["table"]["GeneSymbolList"] = []
                    datalist=pmiddata['MedlineCitation']['GeneSymbolList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("gene"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["GeneSymbolList"].append(d)
                # MeshHeading
                if "MeshHeadingList" in pmiddata['MedlineCitation']:
                    dbdata["table"]["MeshHeading"] = []
                    datalist=pmiddata['MedlineCitation']['MeshHeadingList']
                    for j in datalist:
                        d={}
                        d=PubmedDict2TableDict(j, getPubmedDictKeyDir("meshheading"))
                        d["PMID_id"]=int(pmid)
                        dbdata["table"]["MeshHeading"].append(d)
                # Keyword
                if "KeywordList" in pmiddata['MedlineCitation']:
                    datalist=pmiddata['MedlineCitation']['KeywordList']
                    if len(datalist) > 0:
                        dbdata["table"]["Keyword"] = []
                        for j in datalist[0]:
                            d={}
                            d=PubmedDict2TableDict(j, getPubmedDictKeyDir("keyword"))
                            d["PMID_id"]=int(pmid)
                            dbdata["table"]["Keyword"].append(d)
            dbdataarray.append(dbdata)
    return dbdataarray

def savePMIDdata(pmiddata):
    dblist = getPubmedTables()
    pt("Preparing empty bulk tables...")
    tabledata = {}
    for table in dblist:
        tabledata[table] = []
    pt("Updating old data and collecting new data...")
    for dbdata in pmiddata:
        if dbdata["indb"]: 
            for table in dbdata["table"]:
                dblist[table].objects.filter(id=dbdata["pmid"]).update(**dbdata["table"][table])
        else:
            for table in dbdata["table"]:
                if str(type(dbdata["table"][table])).find("list") != -1:
                    tabledata[table].extend(dbdata["table"][table])
                else:
                    tabledata[table].append(dbdata["table"][table])
    pt("Writing new data into database...")
    # save the pubmed first
    if len(tabledata["Pubmed"]) > 0:
        pt("Saving Pubmed...")
        try:
            writeIntoDBBulk(dblist["Pubmed"],tabledata["Pubmed"])
        except: 
            pt("in the single insert loop")
            writeIntoDBSingleInsert(dblist["Pubmed"],tabledata["Pubmed"]) 
    # save other data
    for table in dblist:
        if table != "Pubmed":
            if len(tabledata[table]) > 0:
                pt("Saving %s..." % table)
                writeIntoDBBulk(dblist[table],tabledata[table])

def checkPMIDExists(pmidarray):
    # input pmidarray
    # output array of new and old pmids
    pt("Checking how many PMIDs are in the database...")
    newpmids=[]
    oldpmids=[]
    for pmid in pmidarray:
        if pmid != "":
            if Pubmed.objects.filter(id=pmid).count() == 0:
                if not pmid in newpmids:
                    newpmids.append(pmid)
            else:
                if not pmid in oldpmids:
                    oldpmids.append(pmid)
    pt("%s new pmids, %s old pmids" % (len(newpmids),len(oldpmids)))
    return (newpmids,oldpmids)

def deletePMIDs(pmidarray):
    # delete the record for one pmid, or pmids separated by ","
    for pmid in pmidarray:
        Pubmed.objects.filter(id=pmid).delete()
        
def webenv2PMIDarray(query_key,webenv,ret_start,ret_max):
    results = Entrez.read(Entrez.esummary(db="pubmed", retmode="xml", retstart = ret_start, retmax= ret_max, webenv=webenv, query_key= query_key))
    print results

def extractPubmedDate(ob):
    # get the data from xml element
    d=""
    f=""
    if 'Year' in ob:
        d=str(ob['Year'])
        f="%Y"
    if 'Month' in ob:
        d=d+"-"+str(ob['Month'])
        if len(str(ob['Month'])) == 3:
            f=f+"-%b"
        else:
            f=f+"-%m"
    if 'Day' in ob:
        d=d+"-"+str(ob['Day'])
        f=f+"-%d"
    if d != "":
        wrongdate = {}
        wrongdate["2014-Feb-31"] = "2014-Feb-28"
        wrongdate["2014-Feb-30"] = "2014-Feb-28"
        wrongdate["2014-Apr-31"] = "2014-Apr-30"
        wrongdate["2014-11-31"] = "2014-11-30"
        if d in wrongdate:
            d = wrongdate[d]
        try:
            dt=datetime.datetime.strptime(d, f)
        except:
            pt("Wrong date: %s" % d,"red")
        d=dt.strftime('%Y-%m-%d')
    return (d)
################################################################################
# work flows
def downloadPMIDs(pmidarray,*args, **kwargs):
    def split(arr, size):
         arrs = []
         while len(arr) > size:
             pice = arr[:size]
             arrs.append(pice)
             arr   = arr[size:]
         arrs.append(arr)
         return arrs
    # pmidarray
    added = 0
    pmids = []
    if len(args)>0:
        if len(pmidarray) > 0:
            pmids = pmidarray
    else:
        (newpmids,oldpmids) = checkPMIDExists(pmidarray)
        if len(newpmids) > 0:
            pmids = newpmids
    added = len(pmids)
    if added > 0:
        gcount = 500
        pmidgroup = split(pmids,gcount)
        c = 1
        for g in pmidgroup:
            pt("Processing PMIDs %s - %s..." % (c, c + len(g) - 1))
            pmiddata = []
            pmiddata.extend(pmid2DBdata(g))
            try:
               savePMIDdata(pmiddata)
            except:
               pt("could not save")   
            c = c + gcount
    return added
        
def downloadPMIDs_recent(*args, **kwargs):
    # put argument to get the download number or not
    pt("Downloading the recent pubmed data...")
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "pubmed_download_recent"
    search_results = Entrez.read(Entrez.esearch(db="pubmed",reldate=1, datetype="edat", term="",usehistory="y",retmax=100000))
    count = int(search_results["Count"])
    if len(args)>0:
        return count
    else:
        pmidarray = search_results["IdList"]
        pt("Found %i results" % count)
        added = downloadPMIDs(pmidarray)
        dlog["description"] = "<b>%s</b> recent papers were downloaded" % added
        dlog["count"] = added
        dlog["success"] = True
        write_cron_job_log(dlog)
    
def downloadPMIDs_batch(data):
    # overwrite = true/false, true to redownload all data for one particular month
    # dtend = new end date, 2011-01-01
    # days = how many days to download, only available when enddate were set
    overwrite = data[0]
    if overwrite:
        action = "update"
    else:
        action = "batch"
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "pubmed_download_%s" % action    
    dtend = data[1]
    days = data[2]
    pt("Starting downloading batch pubmed data, overwrite:%s, dtend:%s, days:%s" % (overwrite, dtend, days))
    # get the start and end date
    dtstart = None
    data={}
    if dtend:
        dtend = datetime.datetime.strptime(dtend, '%Y-%m-%d')
        if days:
            dtstart = dtend - relativedelta(days=(days-1))
    else:
        if CronJobLog.objects.filter(app="pubmed_download_%s" % action).count() == 0:
            pt("Not found any log for pubmed_download_%s, use today's date" % action)
            dtend = datetime.datetime.utcnow().replace(tzinfo=utc)
        else:
            log = CronJobLog.objects.filter(app="pubmed_download_%s" % action).order_by("dtstart")[:1]
            dtend = log[0].dtstart
            pt("dtend: %s" % str(dtend))
    if not dtstart:
        if dtend.day == 1:
            dtstart = dtend - relativedelta(months=1)
        else:
            dtstart=dtend
            dtstart = datetime.datetime.strptime(str(dtstart.year)+"/"+str(dtstart.month)+"/01", '%Y/%m/%d')
    #startdate = datetime.strptime("2013/12/25", '%Y/%m/%d')
    #enddate = datetime.strptime("2013/12/25", '%Y/%m/%d')
    enddate = dtend.strftime('%Y/%m/%d')
    startdate = dtstart.strftime('%Y/%m/%d')
    pt("Downloading pubmed data for %s-%s..." % (startdate,enddate),True)
    search_results = Entrez.read(Entrez.esearch(db="pubmed",mindate=startdate,maxdate=enddate,datetype="edat", term="",usehistory="y", retmax=100000))
    count = int(search_results["Count"])
    pmidarray = search_results["IdList"]
    pt("Found %i results" % count)
    if overwrite:
        added = downloadPMIDs(pmidarray,True)
    else:
        added = downloadPMIDs(pmidarray)
        print added
    dlog["description"] = "Downloaded the batch pubmed papers back from %s for %s days, overwrite old data: %s" % (dtend,days,overwrite)
    dlog["count"] = added
    dlog["success"] = True
    write_cron_job_log(dlog)

def collect_paper_count():
    d = {}
    d["count"] = downloadPMIDs_recent("true")
    d["dtstamp"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    writeIntoDB(Paper_count,d)

def test():
    pmidarray = ["25434055"]
    deletePMIDs(pmidarray)
    downloadPMIDs(pmidarray)

def doi2url(data):
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "pubmed_doi2url_recent"
    # dtend, provide an end time, use recent to parse recent 30 days
    # days
    dtend = data[0]
    days = data[1]
    dtstart = None
    data={}
    if dtend:
        if dtend == "recent":
            dtend = datetime.datetime.now()
            if not days:
                days = 30
        else:
            dtend = datetime.datetime.strptime(dtend, '%Y-%m-%d')
        if days:
            dtstart = dtend - relativedelta(days=(days-1))
    else:
        if CronJobLog.objects.filter(app="pubmed_doi2url").count() == 0:
            pt("Not found any log for pubmed_doi2url, use today's date")
            dtend = datetime.datetime.utcnow().replace(tzinfo=utc)
        else:
            log = CronJobLog.objects.filter(app="pubmed_doi2url").order_by("dtstart")[:1]
            dtend = log[0].dtstart
            pt("dtend: %s" % str(dtend))
    if not dtstart:
        if dtend.day == 1:
            dtstart = dtend - relativedelta(months=1)
        else:
            dtstart=dtend
            dtstart = datetime.datetime.strptime(str(dtstart.year)+"/"+str(dtstart.month)+"/01", '%Y/%m/%d')

    from journals.views import DOI2URL
    # collect the done doi
    pt("Clean old none url doi...")
    n = datetime.datetime.now() - relativedelta(months=1)
    PaperUrl.objects.filter(url__isnull=True,dtstamp__lt=n.strftime('%Y-%m-%d')).delete()
    pt("Clean short url links...")
    for p in ShortPattern.objects.filter(cat="INCLUDE",inuse=1):
        PaperUrl.objects.filter(url__icontains=p.pattern).delete()
    pt("Collecting the finished doi...")
    ob = Pubmed.objects.filter(paperurl__isnull=True, DOI__isnull=False,DateCreated__range=(dtstart.strftime('%Y-%m-%d'),dtend.strftime('%Y-%m-%d'))).exclude(DOI = 'NULL')
    count = ob.count()
    pt("Number of doi not transformed: %s between %s to %s" % (count,dtstart.strftime('%Y-%m-%d'),dtend.strftime('%Y-%m-%d')))
    if count > 0:
        dois = {}
        for o in ob:
            if o.DOI != "NULL":
                dois[o.DOI] = o.id
        i = PaperUrl.objects.latest('id').id
        out = settings.BASE_DIR + "/tmp/paperurl.csv"
        rm(out)
        fh = open(out,"w+")
        for doi in dois:
            url = DOI2URL(doi,"long")
            d = {}
            if url:
                url = twitter_expanded_url(url)
                if url:
                    d["url"] = url
            d["PMID"] = dois[doi]
            d["DOI"] = doi
            d["dtstamp"] = datetime.datetime.now().strftime('%Y-%m-%d')
            d["id"] = i + 1
            i = i + 1
            fh.write(getCSVdata(["id","PMID","DOI","url","dtstamp"],d))
            count -= 1
            if str(count)[len(str(count))-2:] == "00":
                pt("%s left" % count,True)
        fh.close()
        pt("Loading into database...")
        # wake up the database
        a = Pubmed.objects.all().count()
        query = "LOAD DATA LOCAL INFILE \'" + out + "\' REPLACE INTO TABLE pubmed_paperurl FIELDS TERMINATED BY '\t' ESCAPED BY '' LINES TERMINATED BY '\n' (id, PMID_id, DOI, url, @dtstamp) SET dtstamp = STR_TO_DATE(@dtstamp, '%Y-%c-%e')"
        run_mysql_script("SET FOREIGN_KEY_CHECKS = 0;")
        run_mysql_script(query)
        run_mysql_script("SET FOREIGN_KEY_CHECKS = 1;")
        rm(out)
        del dois
    # write comment
    dlog["description"] = "Get the url for pubmed doi between %s and %s, count <b>%s</b>" % (dtstart.strftime('%Y-%m-%d'),dtend.strftime('%Y-%m-%d'),count)
    write_cron_job_log(dlog)

def doiurl_correction(url):
	# url = none. do all
	# url = url, only on perticular url
	# url = "INCLUDE", all include url
	if url == None:
		ob = PaperUrl.objects.filter(url__isnull=True)
	elif url == "INCLUDE":
		urls = ShortPattern.objects.filter(cat="INCLUDE",inuse=1)
		q_sum = Q()
		for u in urls:
			q_sum.add(Q(url__icontains=u.pattern),Q.OR)
		ob = PaperUrl.objects.filter(q_sum)
	else:
		ob = PaperUrl.objects.filter(url__icontains=url)
	count = ob.count()
	pt("Number of urls to transform: %s" % count)
	u = {}
	for o in ob:
		count -= 1
		if o.url:
			if not o.url in u:
				url = twitter_expanded_url(o.url)
				u[o.url] = url
			else:
				url = u[o.url]
			if url != o.url: # incase of value = none
				pt("%s left" % count)
				o.url = url
				o.save()
	# write comment
	d = {}
	d["app"] = "pubmed_doiurl_correction"
	d["description"] = "Correct the transformed doi urls"
	write_cron_job_log(d)

def doi_null_fix():
    q_sum = Q()
    q_sum.add(Q(DOI__isnull=True),Q.OR)
    q_sum.add(Q(DOI="NULL"),Q.OR)
    ob = Pubmed.objects.filter(q_sum).order_by("-DateCreated")[:10000]
    count = ob.count()
    for o in ob:
        count = count - 1
        pt("Processing %s, %s left..." % (o.id,count))
        deletePubmedData(o.id)
        downloadPMIDs(o.id)
    
def summary_date(data):
    # start date, can be none, xxxx-xx-xx
    # end date, cna be none
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "pubmed_summary_date"
    sd = data[0]
    ed = data[1]
    if sd:
        sd = datetime.datetime.strptime(sd, "%Y-%m-%d")
    else:
        sd = datetime.datetime.strptime("1900-01-01", "%Y-%m-%d")
    if ed:
        ed = datetime.datetime.strptime(ed, "%Y-%m-%d")
    else:
        ed = datetime.datetime.now()
    pt("Starting count summary for %s to %s..." % (sd.strftime('%Y-%m-%d'),ed.strftime('%Y-%m-%d')),True)
    cd = sd
    db = {}
    while cd <= ed:
        d = {}
        d["count"] = Pubmed.objects.filter(DateCreated=cd.strftime('%Y-%m-%d')).count()
        d["DateCreated"] = cd
        pt("%s (%s)" % (cd.strftime('%Y-%m-%d'),d["count"]))
        if Summary_date.objects.filter(DateCreated=cd.strftime('%Y-%m-%d')).count() >0:
            Summary_date.objects.filter(DateCreated=cd.strftime('%Y-%m-%d')).update(**d)
        else:
            writeIntoDB(Summary_date,d)
        cd = cd + relativedelta(days=1)
    # write comment
    dlog["description"] = "Summarized number of papers by date"
    write_cron_job_log(dlog)
        
def summary_pmid_range(data):
    # start date, can be none, xxxx-xx-xx
    # end date, cna be none
    sd = data[0]
    ed = data[1]
    if sd:
        sd = datetime.datetime.strptime(sd, "%Y-%m-%d")
    else:
        sd = datetime.datetime.strptime("1900-01-01", "%Y-%m-%d")
    if ed:
        ed = datetime.datetime.strptime(ed, "%Y-%m-%d")
    else:
        ed = datetime.datetime.now()
    pt("Starting pmid range summary for %s to %s..." % (sd.strftime('%Y-%m-%d'),ed.strftime('%Y-%m-%d')),True)
    cd = sd
    f = settings.BASE_DIR + "/tmp/summary_pmid_range.txt"
    rm(f)
    fh = open(f,"w+")
    while cd <= ed:
        cd2 = cd + relativedelta(months=1)
        count = Pubmed.objects.filter(DateCreated__range=(cd.strftime('%Y-%m-%d'),cd2.strftime('%Y-%m-%d'))).count()
        if count > 0:
            sm = Pubmed.objects.filter(DateCreated__range=(cd.strftime('%Y-%m-%d'),cd2.strftime('%Y-%m-%d'))).order_by("PMID")[:1][0].PMID
            lg = Pubmed.objects.filter(DateCreated__range=(cd.strftime('%Y-%m-%d'),cd2.strftime('%Y-%m-%d'))).order_by("-PMID")[:1][0].PMID
        else:
            sm = "None"
            lg = "None"
        fh.write(cd.strftime('%Y-%m-%d') + "\t" + sm + "\t" + lg + "\n")
        pt("%s" % cd.strftime('%Y-%m-%d'))
        cd = cd + relativedelta(months=1)
    fh.close()

################################################################################
def upload2s3(source_path,target):
    import math
    from filechunkio import FileChunkIO
    pt("uploading to S3...",False)
    source_size = os.stat(source_path).st_size
    mp = bucket.initiate_multipart_upload(target)
    chunk_size = 52428800
    chunk_count = int(math.ceil(source_size / chunk_size))
    for i in range(chunk_count + 1):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(source_path, 'r', offset=offset,bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
        
@staff_member_required
def addRecordFromWeb(request):
    data = {}
    data["title"] = 'Add PMIDs'
    data["form_wide"] = True
    data["form_action"] = reverse("pubmed_add")
    if request.method == 'POST':
        data["form"] = PMIDForm(request.POST)
        if data["form"].is_valid():
            pmids = request.POST.get('PMIDs','')
            pmids = re.sub("[\n\r\s]+","",pmids)
            downloadPMIDs(pmids.split(","))
            return HttpResponseRedirect(reverse('backend_home'))
    else:
        data["form"] =PMIDForm()
    return render_to_response("base_form.html", data, context_instance=RequestContext(request))

@staff_member_required
def deleteRecordFromWeb(request):
    data = {}
    data["title"] = 'Delete PMIDs'
    data["form_wide"] = True
    data["form_action"] = reverse("pubmed_delete")
    if request.method == 'POST':
        data["form"] = PMIDForm(request.POST)
        if data["form"].is_valid():
            pmids = request.POST.get('PMIDs','')
            pmids = re.sub("[\n\r\s]+","",pmids)
            deletePMIDs(pmids.split(","))
            return HttpResponseRedirect(reverse('backend_home'))
    else:
        data["form"] =PMIDForm()
    return render_to_response("base_form.html", data, context_instance=RequestContext(request))

def updatePubmedSummaryTable():
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "pubmed_summary"
    oblist=getPubmedTables()
    for ob in oblist:
        d = {}
        d["db"] = ob
        d["count"] = oblist[ob].objects.all().count()
        if Summary.objects.filter(db = ob).count() > 0:
            Summary.objects.filter(db = ob).update(**d)
        else:
            writeIntoDB(Summary,d)
    # write comment
    dlog["description"] = "Summarized number of reacords for each pubmed table"
    write_cron_job_log(dlog)

@staff_member_required
def home(request):
    data = {}
    data["tlr_sb"] = "pubmed/sidebar.html"
    return render_to_response('base_lr.html', data, context_instance=RequestContext(request))

@staff_member_required
def summary(request):
    data={}
    data['count'] = Summary.objects.all().order_by("db")
    return render_to_response("pubmed/summary.html", data, context_instance=RequestContext(request))

@staff_member_required
def pubmed_cronjob_log(request):
    data = {}
    jobs = ["pubmed_download_recent",
            "pubmed_parse_recent",
            "pubmed_download_update",
            "pubmed_parse_update",
            "pubmed_summary_date",
            "pubmed_summary",
            "pubmed_doi2url"
            ]
    for j in jobs:
        data[j] = CronJobLog.objects.filter(app=j).order_by("-dtstamp")[:10]
    return render_to_response('pubmed/log.html', data, context_instance=RequestContext(request))

@staff_member_required
def plot(request):
    data={}
    return render_to_response('pubmed/plot.html', data, context_instance=RequestContext(request))

@staff_member_required
def plot_view(request,granularity,start,end):
    data={}
    data["data"]=[]
    startdate = datetime.datetime.strptime(start, "%Y-%m-%d")
    enddate = datetime.datetime.strptime(end, "%Y-%m-%d")
    sd = None
    ed = None
    if ed == None:
        if granularity == "yearly":
            ed = datetime.datetime.strptime(str(startdate.year+1)+"-01-01", "%Y-%m-%d")
        elif granularity == "monthly":
            ed = datetime.datetime.strptime(str(startdate.year)+"-"+str(startdate.month+1)+"-01", "%Y-%m-%d")
        elif granularity == "daily":
            ed = startdate
        sd = startdate
    while sd < enddate:
        print "%s to %s" % (sd,ed)
        count = {}
        count["y"] = Summary_date.objects.filter(DateCreated__range=(sd.strftime('%Y-%m-%d'),ed.strftime('%Y-%m-%d'))).aggregate(Sum('count'))["count__sum"]
        if count["y"] == None:
            count["y"] = 0
        search_results = Entrez.read(Entrez.esearch(db="pubmed",
                                                    mindate=sd.strftime('%Y/%m/%d'),maxdate=ed.strftime('%Y/%m/%d'),
                                                    datetype="edat", term="",
                                                    usehistory="y", retmax=1))
        count["z"] = int(search_results["Count"])
        if granularity == "yearly":
            count["x"] = sd.strftime('%Y')
            sd = ed
            ed = ed + relativedelta(years=1)
        elif granularity == "monthly":
            count["x"] = sd.strftime('%Y-%m')
            sd = ed
            ed = ed + relativedelta(months=1)
        elif granularity == "daily":
            count["x"] = sd.strftime('%Y-%m-%d')
            sd = sd + relativedelta(days=1)
            ed = sd
        data["data"].append(count)
        if ed > enddate:
            ed = enddate
    return render_to_response('pubmed/plot_view.html', data, context_instance=RequestContext(request))
