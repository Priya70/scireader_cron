from django.conf import settings
from django.utils.timezone import utc
from pubmed.views import *
from biorxiv.views import *
from arxiv.views import *
from backend.views import create_LDA_output, lda_pipeline, lda_import_scores_s3
from backend.models import CronJobStatus, CronJobLog
from journals.views import updateJournalDB, updateJournalURL, generateRecentJournalPapers
from twitter.views import download_tweets, twitter_pmid, do_twitter_expanded_url, twitter_summary_count, get_debug_url, twitter_summary_one, collect_top_tweets_papers, filter_twitter_accounts
from recommendations.views import recom_import_s3, recom_error_report, recom_email_weekly,recom_email_weekly_test
from library.forms import parseUploadedlibFile
from common.common import pt, writeIntoDB, mem
from emails.views import send_emails
from dateutil.relativedelta import relativedelta
import time, os, sys, datetime, boto
from time import strftime
from boto.s3.connection import S3Connection
from boto.s3.key import Key

cores = 1
    
def autoRunList():
    auto = {}
    args = {}
    # download pubmed recent papers
    auto["pubmed_download_recent"] = downloadPMIDs_recent
    # download pubmed old papers by batch
    # 30 minutes per month
    # 8 hours can download 16 month in max
    auto["pubmed_download_update"] = downloadPMIDs_batch
    args["pubmed_download_update"] = [True,None,None]
    auto["pubmed_download_batch"] = downloadPMIDs_batch
    args["pubmed_download_batch"] = [False,None,None]  #to download specific dates ,run on the python command line ie in manage.py shell for eg:downloadPMIDs_batch([False,'2015-07-14',14]) will download for 14 days prior to 2015-07-14.
    auto["pubmed_download_tmp"] = downloadPMIDs_batch # #default downloads from the first of the month
    args["pubmed_download_tmp"] = [True,"2014-08-28",None]
    # remove duplicated pmids
    # auto["pubmed_remove_duplicate"] = removeDuplicatedPMIDs
    # pubmed date summary
    auto["pubmed_summary_date"] = summary_date
    args["pubmed_summary_date"] = [None,None]
    # summary the pubmed
    auto["pubmed_summary"] = updatePubmedSummaryTable
    # pubmed paper coun
    auto["pubmed_collect_paper_count"] = collect_paper_count
    
    # lda
    auto["lda_all"] = create_LDA_output
    args["lda_all"] = "all"
    auto["lda_recent"] = create_LDA_output
    args["lda_recent"] = "recent"
    auto["lda_update"] = lda_pipeline
    auto["lda_import_scores_s3"] = lda_import_scores_s3
    
    # personal recommendaiton
    auto["recom_import_s3"] = recom_import_s3
    
    # recommendation error report
    auto["recom_error_report"] = recom_error_report
    
    # recom_eamil_weekly
    auto["recom_email_weekly"] = recom_email_weekly
    auto["recom_email_weekly_test"] = recom_email_weekly_test
    
    # upload library
    auto["lib_import"] = parseUploadedlibFile
    
    # updated the journals
    auto["journals_db"] = updateJournalDB
    auto["journals_url"] = updateJournalURL
    auto["journals_papers"] = generateRecentJournalPapers
    # twitter
    auto["twitter_download"] = download_tweets
    auto["twitter_url"] = do_twitter_expanded_url
    args["twitter_url"] = "INCLUDE"
    auto["twitter_summary_count"] = twitter_summary_count
    auto["twitter_summary_one"] = twitter_summary_one
    args["twitter_summary_one"] = get_debug_url()
    auto["twitter_papers"] = collect_top_tweets_papers
    auto["twitter_accounts_filter"] = filter_twitter_accounts
    auto["pubmed_doi2url"] = doi2url
    args["pubmed_doi2url"] = [None,None]
    auto["pubmed_doi2url_recent"] = doi2url
    args["pubmed_doi2url_recent"] = ["recent",10]
    auto["pubmed_doiurl_correction"] = doiurl_correction
    args["pubmed_doiurl_correction"] = "INCLUDE"
    auto["pubmed_doi2url_tmp"] = doi2url
    args["pubmed_doi2url_tmp"] = ["2014-09-20",30]
    auto["pubmed_doi_null"] = doi_null_fix
    auto["twitter_pmid"] = twitter_pmid
    args["twitter_pmid"] = None
    auto["twitter_pmid_one"] = twitter_pmid
    args["twitter_pmid_one"] = get_debug_url()
    
    # download arxiv data
    auto["arxiv_download"] = checkArxivUpdates
    auto["arxiv_parse"] = parseArxivXML
    # get the summary table
    auto["arxiv_summary"] = updateArxivSummaryTable
    # download biorxiv data
    auto["biorxiv_download"] = readBiorxivXML


    # backup
    auto["backup"] = backup
    # pmid range
    auto["summary_pmid_range"] = summary_pmid_range
    args["summary_pmid_range"] = [None, None]
    # organize pubmed download
    #auto["organize_download"] = organize_download
    # report
    auto["report"] = report
    args["report"] = "email"
    # cron job check
    auto["job_status"] = CheckCronJobStatus
    
    # auto pipelines
    # monthly, crontype=-1,runtimes=1,Y=-1, M=-1
    # daily, crontype=-1,runtimes=1,Y=-1,M=-1,D=-1
    # hourly, crontype=-1,runtimes=1,Y=-1,M=-1,D=-1,H=-1
    # once, crontype=1,runtimes=1
    # nonstop, crontype=-1,runtimes=-1
    auto["timerange"] = CheckCronJobTimeRange
    args["timerange"] = ["-1:-1:-1:-1:00:00","-1:-1:-1:-1:05:00"]
    
    auto["cron_hourly"] = RepeatPipeline
    #args["cron_hourly"] = ["cron_hourly",-1,1,"-1:-1:-1:-1:00:00","-1:-1:-1:-1:05:00"]
    args["cron_hourly"] = ["cron_hourly",-1,3,"-1:-1:-1:10:00:00","-1:-1:-1:16:05:00"]
    auto["cron_daily"] = RepeatPipeline
    args["cron_daily"] = ["cron_daily",-1,1,"-1:-1:-1:18:00:00","-1:-1:-1:18:30:00"]
    auto["cron_monthly"] = RepeatPipeline
    args["cron_monthly"] = ["cron_monthly",-1,1,"-1:-1:1:19:00:00","-1:-1:2:19:00:00"]
    auto["cron_nonstop"] = RepeatPipeline
    args["cron_nonstop"] = ["www_nonstop",-1,-1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["cron_nonstop_night"] = RepeatPipeline
    args["cron_nonstop_night"] = ["cron_nonstop_night",-1,-1,"-1:-1:-1:18:00:00","-1:-1:-1:02:00:00"]
    auto["cron_nonstop"] = RepeatPipeline
    args["cron_nonstop"] = ["cron_nonstop",-1,-1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_daily"] = RepeatPipeline
    args["www_daily"] = ["www_daily",-1,1,"-1:-1:-1:08:00:00","-1:-1:-1:08:01:00"]
    auto["www_hourly"] = RepeatPipeline
    args["www_hourly"] = ["www_hourly",-1,1,"-1:-1:-1:-1:00:00","-1:-1:-1:-1:01:00"]
    auto["www_nonstop1"] = RepeatPipeline
    args["www_nonstop1"] = ["www_nonstop1",-1,-1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_nonstop2"] = RepeatPipeline
    args["www_nonstop2"] = ["www_nonstop2",-1,-1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_jobstatus"] = RepeatPipeline
    args["www_jobstatus"] = ["www_jobstatus",-1,1,"-1:-1:-1:16:00:00","-1:-1:-1:16:01:00"]
    #args["www_jobstatus"] = ["www_jobstatus",-1,1,"-1:-1:-1:-1:00:00","-1:-1:-1:-1:01:00"]
    
    auto["cron_hourly_test"] = RepeatPipeline
    args["cron_hourly_test"] = ["cron_hourly",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["cron_daily_test"] = RepeatPipeline
    args["cron_daily_test"] = ["cron_daily",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["cron_monthly_test"] = RepeatPipeline
    args["cron_monthly_test"] = ["cron_monthly",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["cron_nonstop_night_test"] = RepeatPipeline
    args["cron_nonstop_night_test"] = ["cron_nonstop_night",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["cron_nonstop_test"] = RepeatPipeline
    args["cron_nonstop_test"] = ["cron_nonstop",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_daily_test"] = RepeatPipeline
    args["www_daily_test"] = ["www_daily",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_nonstop_test"] = RepeatPipeline
    args["www_nonstop_test"] = ["www_nonstop",1,1,"-1:-1:-1:-1:-1:-1","-1:-1:-1:-1:-1:-1"]
    auto["www_weekly"] = RepeatPipeline
    args["www_weekly"] = ["www_weekly",-1,1,"-1:-1:-1:22:10:00","-1:-1:-1:22:11:00"]
    return auto,args

jobs = {}

jobs["cron_hourly"] = [
    "lda_import_scores_s3", # import the topic papers
    "twitter_download" # download the twitter for each journal

]

jobs["cron_daily"] = [
    "pubmed_download_recent",# download pubmed papers
    "arxiv_download",# download arxiv papers
    "biorxiv_download",# download biorxiv papers
    "journals_papers",# download journal papers
    "pubmed_summary",# summary of number of records from each table
    'pubmed_summary_date',# Summary pubmed papers by date
    "pubmed_doi2url_recent",# get the url for new papers
    "twitter_pmid", # link the twitter with pmid
    "twitter_papers", # collect the top twittered papers
    "twitter_summary_count", # summary of the number of tweets been linked
    "arxiv_parse", # parse the arxiv data
    "arxiv_summary",# summary for arxiv tables
    "recom_error_report", #recommendation error report
    "backup" # backup the website
    ]

jobs["cron_monthly"] = [
    
]

jobs["cron_nonstop_night"] = [

]

jobs["cron_nonstop"] = [

]

jobs["www_weekly"] = [
    "recom_email_weekly_test" # send out the cron job report
    "recom_email_weekly" # send out the cron job report    
]

jobs["www_daily"] = [
    "report" # send out the cron job report
]

jobs["www_hourly"] = [
    "pubmed_collect_paper_count" # get pubmed paper count
]

jobs["www_nonstop1"] = [
    "lib_import", # import the papers for library import action, and do the immediate recommendations
]

jobs["www_nonstop2"] = [
    "recom_import_s3" # import the recommendation papers
]

jobs["www_jobstatus"] = [
    "job_status" # send out the cron job status report
]

def CheckCronJobTimeRange(data):
    #dtstart, Y:M:D:H:M:S, use -1 to skip
    #dtend, Y:M:D:H:M:S, use -1 to skip
    #week days, 0 for Monday, -1 for any
    dtstart = data[0] #, Y:M:D:H:M:S, use -1 to skip
    dtend = data[1] #, Y:M:D:H:M:S, use -1 to skip
    wk = data[2]
    s = dtstart.split(":")
    e = dtend.split(":")
    cycle = [0,12,30,24,60,60]
    sec = [365*24*60*60,30*24*60*60,24*60*60,60*60,60,1]
    now = datetime.datetime.now()
    dtnow = now.strftime('%Y:%m:%d:%H:%M:%S')
    #pt("dtstart: %s, dtend: %s, dtnow: %s" % (dtstart,dtend,dtnow),True)
    n = dtnow.split(":")
    ds = now
    de = now
    for i in range(0,6):
        if int(s[i]) != -1:
            seconds = (int(n[i]) - int(s[i])) * sec[i]
            ds = ds - relativedelta(seconds=seconds)
        if int(e[i]) != -1:
            if int(e[i]) < int(s[i]):
                if int(e[i-1]) == -1 and int(s[i-1]) == -1:
                    seconds = (int(n[i]) - int(e[i]) - cycle[i]) * sec[i]
            else:
                seconds = (int(n[i]) - int(e[i])) * sec[i]
            de = de - relativedelta(seconds=seconds)
        #pt(i)
        #pt("%s-%s" % (ds,de))
    #pt("%s-%s" % (ds,de))
    if now >= ds and now <= de:
        if wk == -1 or wk == now.weekday():
            return True
        return False
    else:
        return False

def WriteCronJobStatus(job):
    d = {}
    d["job"] = job
    d["dtstamp"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    d["mem"] = mem()
    if CronJobStatus.objects.filter(job=job).count() > 0:
        CronJobStatus.objects.filter(job=job).update(**d)
    else:
        writeIntoDB(CronJobStatus,d)
    
def CheckCronJobStatus():
    d = {}
    d["jobs"] = CronJobStatus.objects.all()
    d["templatename"] = "cron_job_status"
    d["to_email"] = ["ntelis@stanford.edu","prd@stanford.edu"]
    d["duplicated_check"] = False
    send_emails(d)

def RepeatPipeline(data):
    from backend.models import CronJobLog
    # the script will trick the events at specific time for specific jobs
    jobname = data[0] #, a lit of jobs
    crontype = data[1] #, the type of cron job, -1 (endless), 1 (once), int(some times)
    runtimes = data[2] #, the times to run the cron jobs inside a cycle, -1 (endless), 1 (once), int
    dtstart = data[3] #, Y:M:D:H:M:S, use -1 to skip
    dtend = data[4] #, Y:M:D:H:M:S, use -1 to skip
    job_list = jobs[jobname]
    auto,args = autoRunList()
    cronruns = 0
    while (crontype == -1 or int(cronruns)<int(crontype)):
        jobruns = 0
        if jobname == "www_weekly":
            do = CheckCronJobTimeRange([dtstart,dtend,0])#6 for sunday
        else:
            do = CheckCronJobTimeRange([dtstart,dtend,-1])
        # parse the date
        if do:
            if (runtimes == -1 or int(jobruns)<int(runtimes)):
                cronruns = cronruns + 1
                jobruns = jobruns + 1
                pt("%s at %s times, (%s to %s)" % (jobname,jobruns,dtstart,dtend),True)
                WriteCronJobStatus(jobname)
                failed_jobs = []
                todo_jobs = job_list
                
                for prog in todo_jobs:
                    try:
                        pt("job: %s" % prog,True)
                        if prog in args:
                            auto[prog](args[prog])
                        else:
                            auto[prog]()
                    except Exception, e:
                        pt("failed!\n%s" % str(e),False)
                """
                while len(todo_jobs) > 0:
                    failed_jobs = []
                    for prog in todo_jobs:
                        try:
                            pt("job: %s" % prog,True)
                            if prog in args:
                                auto[prog](args[prog])
                            else:
                                auto[prog]()
                        except Exception, e:
                            failed_jobs.append(prog)
                            pt("failed!\n%s" % str(e),False)
                    todo_jobs = failed_jobs
                    if len(todo_jobs) > 0:
                        pt("Found %s failed jobs" % len(todo_jobs),"red")
                    else:
                        pt("Done","green")
                    time.sleep(30)"""
                pt("Sleeping ... zzZ",False)
            time.sleep(58)
        else:
            time.sleep(58)
            # avoid the mysql sleep
            WriteCronJobStatus(jobname)
            
def backup():
    dlog = {}
    dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
    dlog["app"] = "backup"
    d = {}
    try:
        #import math
        #from filechunkio import FileChunkIO
        tag = strftime("%Y%m%d%H%M%S")
        #tag = "20140609162237"
        pt("backup the database...")
        #os.system("mysqldump -h "+ settings.DATABASES["default"]["HOST"] +" -P "+ settings.DATABASES["default"]["PORT"] +" -u "+ settings.DATABASES["default"]["USER"] +" "+settings.DATABASES["default"]["NAME"]+" > "+settings.BASE_DIR+"/db_backup_scireader.sql")
        os.system("mysqldump -h "+ settings.DATABASES["default"]["HOST"] +" -P "+ settings.DATABASES["default"]["PORT"] +" -u "+ settings.DATABASES["default"]["USER"] +" "+settings.DATABASES["default"]["NAME"]+" > /mnt/scireader/backup/db_backup_scireader.sql")
        pt("backup the twitter_tweets...")
        #os.system("mysqldump -h "+ settings.DATABASES["default"]["HOST"] +" -P "+ settings.DATABASES["default"]["PORT"] +" -u "+ settings.DATABASES["default"]["USER"] +" "+settings.DATABASES["default"]["NAME"]+" twitter_tweets > "+settings.BASE_DIR+"/backup/twitter_tweets_"+tag+".sql")
        os.system("mysqldump -h "+ settings.DATABASES["default"]["HOST"] +" -P "+ settings.DATABASES["default"]["PORT"] +" -u "+ settings.DATABASES["default"]["USER"] +" "+settings.DATABASES["default"]["NAME"]+" twitter_tweets > /mnt/scireader/backup/twitter_tweets_"+tag+".sql")
        pt("backup the files...")
        #os.system("tar zcf ./backup/scireader_backup_%s.tar.gz --exclude=./backup ./" % tag)
        os.system("tar -zcf ./backup/scireader_backup_%s.tar.gz  /mnt/scireader/backup/db_backup_scireader.sql" %tag)
        pt("remove db_backup_scireader.sql...")
        #os.system("rm "+settings.BASE_DIR+"/db_backup_scireader.sql")
        os.system("rm /mnt/scireader/backup/db_backup_scireader.sql")
        # transfer the data into S3
        if settings.S3_DATA:
            source_path = settings.BASE_DIR + '/backup/scireader_backup_%s.tar.gz' % tag
            target = "backup/"+os.path.basename(source_path)
            upload2s3(source_path,target)
            pt("remove the backup file...")
            os.system("rm %s" % source_path)
            
            #source_path = settings.BASE_DIR + '/backup/twitter_tweets_%s.sql' % tag
            source_path = '/mnt/scireader/backup/twitter_tweets_%s.sql' % tag
            target = "backup/"+os.path.basename(source_path)
            upload2s3(source_path,target)
            pt("remove the twitter_tweets file...")
            os.system("rm %s" % source_path)
    except:
        d["success"] = False
    # write comment
    dlog["description"] = "Backup the whole website"
    write_cron_job_log(dlog)

def run(*script_args):
    if len(script_args) == 0:
        sys.exit("Error: missing parameters!")
    prog = script_args[0]
    pt("Starting script %s..." % prog,True)
    auto,args = autoRunList()
    if prog in auto:
        if prog in args:
            auto[prog](args[prog])
        else:
            auto[prog]()
        pt("Done!",False)
        sys.exit()
    else:
        pt("Not found argu: " + prog,False)
        sys.exit()
        
def report(action):
    # email, send the emails
    # function, return data
    class cronlog(object):
        def __init__(self, d):
            if "app" in d:
                self.app = d["app"]
            if "description" in d:
                self.description = d["description"]
            if "dtstart" in d:
                self.dtstart = d["dtstart"]
            if "dtend" in d:
                self.dtend = d["dtend"]
            if "count" in d:
                self.count = d["count"]
            if "success" in d:
                self.success = d["success"]
            if "mem" in d:
                self.mem = d["mem"]
            if "dtstart" in d and "dtend" in d:
                du = d["dtend"] - d["dtstart"]
                if du.seconds > 3600:
                    self.duration = "%.2f hours" % (float(du.seconds)/3600)
                else:
                    self.duration = "%.2f minutes" % (float(du.seconds)/60)
    data = {}
    data["duplicated_check"] = False
    data["from_email"] = settings.DEFAULT_FROM_EMAIL
    #data["from_email"] = settings.EMAIL_HOST_USER
    data["to_email"] = ["ntelis@stanford.edu","prd@stanford.edu","pritch@stanford.edu"]
    #data["to_email"] = ["yongganw@stanford.edu"]
    data["today"] = datetime.datetime.now()
    data["templatename"] = "cron_job_report"
    data["logs"] = []
    t = datetime.datetime.utcnow().replace(tzinfo=utc)
    y = t - relativedelta(days=1)
    js = []
    js.extend(jobs["www_nonstop1"])
    js.extend(jobs["www_nonstop2"])
    js.extend(jobs["cron_hourly"])
    js.extend(jobs["cron_daily"])
    for j in js:
        if CronJobLog.objects.filter(app=j,dtstart__range=(y,t)).count() > 0:
            data[j] = []
            logs = CronJobLog.objects.filter(app=j,dtstart__range=(y,t))
            d = {}
            d["count"] = 0
            for l in logs:
                d["app"] = j
                d["description"] = l.description
                d["dtstart"] = l.dtstart
                d["dtend"] = l.dtend
                if l.count:
                    d["count"] = d["count"] + int(l.count)
                # replacing the count information
                if d["count"] != 0:
                    if d["description"].find("<b>") != -1:
                        tmp = d["description"].split("<b>")
                        d["description"] = tmp[0]
                        tmp = tmp[1].split("</b>")
                        s = ""
                        if len(tmp) == 2:
                            s = tmp[1]
                        d["description"] = "%s<b>%s</b>%s" % (d["description"],'{:,}'.format(d["count"]),s)
                d["success"] = l.success
                d["mem"] = l.mem
            data["logs"].append(cronlog(d))
        else:
            d = {}
            d["app"] = j
            d["description"] = "Not found the job in log"
            d["success"] = False
            data["logs"].append(cronlog(d))
    # create registered user report
    d = {}
    d["app"] = "registered_users"
    from django.contrib.auth.models import User
    d["count"] = User.objects.all().count()
    d["description"] = "Total Registered Users: <b>%s</b>" % d["count"]
    d["success"] = True
    data["logs"].append(cronlog(d))
    
    if action == "email":
        from emails.views import send_emails
        send_emails(data)
    else:
        return data
