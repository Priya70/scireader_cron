from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.timezone import utc
from django.contrib.admin.views.decorators import staff_member_required
from biorxiv.models import *
from common.common import writeIntoDB
from backend.views import write_cron_job_log
from backend.models import CronJobLog
import datetime, rdflib

def category():
	# return a list of biorxiv category
	category = [
		"Biochemistry",
		"Bioengineering",
		"Bioinformatics",
		"Biophysics",
		"Cancer Biology",
		"Cell Biology",
		"Developmental Biology",
		"Ecology",
		"Evolutionary Biology",
		"Genetics",
		"Genomics",
		"Immunology",
		"Microbiology",
		"Molecular Biology",
		"Molecular Medicine",
		"Neurobiology",
		"Neuroscience",
		"Paleontology",
		"Pathology",
		"Pharmacology",
		"Physiology",
		"Plant Biology",
		"Scientific Communication",
		"Synthetic Biology",
		"Systems Biology",
		"Zoology"
	]
	return (category)

def readBiorxivXML():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "biorxiv_download"
	# extract the xml data
	count=0
	cats = category()
	for cat in cats:
		
		# create a url
		url = "http://hwmaint.biorxiv.highwire.org/cgi/collection/rss?coll_alias=" + cat.lower()
		url = url.replace(" ","_")
		
		# parse the rdf data
		g=rdflib.Graph()
		g.load(url)
		data={}
		for s,p,o in g:
			s = str(s)
			if s.find("http://biorxiv.org/cgi/content/short/") != -1: #find the data
				tmp = p.split('/')
				tag = tmp[-1]
				o = str(o)
				if s in data:
					if tag in data[s]:
						d = data[s][tag]
						d.append(o)
						data[s].update({tag:d})
					else:
						data[s].update({tag:[o]})
				else:
					data[s]={tag:[o]}
		
		# write into database
		for s in data:
			if Biorxiv.objects.filter(link = s).count() == 0: # if not exists
				print data[s]["title"]
				count=count+1
				# create the biorxiv database
				tmp = {}
				tmp["DOI"] = data[s]["identifier"][0]
				tmp["link"] = data[s]["link"][0]
				tmp["description"] = data[s]["description"][0]
				tmp["title"] = data[s]["title"][0]
				tmp["dcDate"] = data[s]["date"][0]
				tmp["PubDate"] = data[s]["publicationDate"][0]
				tmp["publisher"] = data[s]["publisher"][0]
				tmp["section"] = data[s]["section"][0]
				tmp["category"] = cat
				i = datetime.datetime.utcnow().replace(tzinfo=utc)
				tmp["DateToScireader"]=str(i.year) + "-" + str(i.month) + "-" + str(i.day)
				writeIntoDB(Biorxiv,tmp)
				# create the author data
				for author in data[s]["creator"]:
					tmp = {}
					tmp["DOI"] = data[s]["identifier"][0]
					tmp["link"] = data[s]["link"][0]
					tmp["name"] = author
					writeIntoDB(Author,tmp)
	# write comment
	dlog["description"] = "<b>%s</b> biorxiv papers were downloaded" % count
	dlog["count"] = count
	write_cron_job_log(dlog)

@staff_member_required
def summary(request,view):
	data={}
	if view == "lr":
		data["tlr_sb"] = "biorxiv/sidebar.html"
		data["tlr_content"] = "biorxiv/summary.html"
		template = 'base_lr.html'
	else:
		template = "biorxiv/summary.html"
	cats = category()
	d={}
	for cat in cats:
		count = Biorxiv.objects.filter(category = cat).count()
		d[cat]=count
	data['summary'] = d
	return render_to_response(template, data, context_instance=RequestContext(request))

@staff_member_required
def biorxiv_cronjob_log(request):
    data = {}
    jobs = ["biorxiv_download"]
    for j in jobs:
        data[j] = CronJobLog.objects.filter(app=j).order_by("-dtstamp")[:10]
    return render_to_response('pubmed/log.html', data, context_instance=RequestContext(request))