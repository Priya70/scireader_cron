from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.timezone import utc
from dateutil.relativedelta import relativedelta
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Count
from django.db.models import Max
from django.db.models import Q
from twython import Twython
from journals.models import Journals
from twitter.models import *
from bs4 import BeautifulSoup
from backend.views import write_cron_job_log
from common.common import writeIntoDB, eid2id, id2eid, pt, writeIntoDBBulk,writeIntoDBSingleInsert
from common.views import schema_load
from pubmed.models import Pubmed, PaperUrl
import datetime, time, urllib2, requests, urllib, tweepy,signal
from twitter.forms import *
from recommendations.models import Recommendations
from feeds.models import Subcategory, MyTopics, Subjects
from accounts.views import add_visiting_record
import urllib,re,time

TWEETS_PER_PAGE = 5
KEY = [
	settings.SOCIAL_AUTH_TWITTER_KEY,
	'Ial0k6s5FUxdvhpzFmnaFQ',
	'S2duOJCaRiJsjrObe45xtAxZ3', #1
	'dMxCGbU657RMSU8Urc0GD9CrQ', #2
	'4GV1qHrRuQIxIYKmKmtJ9SUwH', #3
	'A3m4hfhsSH1j5TvIvbuWn1ALM', #4
	'ZyJMCHxhKyRLiJhnvEpjT4aDh', #5
	'CdzdJaau8QmMISfso2oflNtnH', #6
	'XZ4Av1uIBTu6T73xQ8oxwJO6K', #7
	'Fawl4GMyyXzNHpDeoJfVYaq18', #8
	'ap0EAWyFbBV6xsXKy8iQUNspx' #9
	]
SECRET = [
	settings.SOCIAL_AUTH_TWITTER_SECRET,
	'iv5u06gwgOSxdzXHuhz4lvgpKfo8fRAnWgJ5rcoxG1Y',
	'qjID44VOnaFSrf3isOL0YzhgdOIhiW5GZ5StdkrGh60trIrveJ',#1
	'JISYRcEiZGp04yDyK8sqD56MbQfbvUNo1oNv8SsQdyo2BFXeW6', #2
	'NvqMIalQNKc04HIw81VJonit8trHIQLq85u5ofEn0EiiwgCyOg', #3
	'WY27QDCI46miyQBXB3TvwOpyiiAETkIAX2suoCE2v3hBTI9LU3', #4
	'Iw5jtgZEmZMq8mA0HNaMIqCTiFJ804UXp4wbdhklJ61G23aj0Z', #5
	'MYdchZXuPHOToBtIwX3YMPFRkFZUWmo21saiV5nQIBStRU8J4i', #6
	'PuD19GwiMsaHzOlgwXnsoXZQrFPIAqGDcoa2sJdIJ1ob1RTlWR', #7
	'2wMa87WYEIBLoVH3VPFNvQjH9Gifk0NS7grtaIM4ekf1dYUOlb', #8
	'EX0TOSoXfjm3a1wtxub3730ahjSU7L9dtoH8fGUsPhITXdNxlM' #9
	]

def get_debug_url():
	if TweetsSummary.objects.filter(debug=True).count() > 0:
		debug_url = TweetsSummary.objects.get(debug=True).url
	else:
		debug_url = "nature.com"
	return debug_url

def twitter_login(i):
	# which account, start 0
	# twython
	"""twitter = Twython(KEY[i], SECRET[i], oauth_version=2)
	ACCESS_TOKEN = twitter.obtain_access_token()
	twitter = Twython(KEY[i], access_token=ACCESS_TOKEN)"""
	# tweepy
	twitter = tweepy.OAuthHandler(KEY[i], SECRET[i])
	return twitter

i = 0
twitter = twitter_login(i)

def urlShort2Long(url):
	#def signal_handler(signum, frame):
	#	raise Exception("Timed out!")
	#signal.signal(signal.SIGALRM, signal_handler)
	#signal.alarm(30)
	
	u = None
	try:
		#f = urllib.urlopen(url)
		#u = f.geturl()
		#f.close()
		resp = requests.head(url,timeout=30)
		u = resp.headers["location"]
	except Exception, msg:
		#pt("not found url for %s" % url,"red")
		a = 1
	return u

def is_short_url(url):
	#pattern = [".org",".com",".net","//www.",".edu",".gov",".pdf",".info",".ca","kr"]
	#ex = ["nature.com/doifinder/","go.nature.com","dx.doi.org"] # expanded
	short = True
	for p in ShortPattern.objects.filter(cat="EXCLUDE"):
		if url.find(p.pattern) != -1:
			short = False
	for e in ShortPattern.objects.filter(cat="INCLUDE"):
		if url.find(e.pattern) != -1:
			short = True
	return short

def twitter_expanded_url(expanded_url):
	# convert the short expanded_url to long url
	url = expanded_url
	if expanded_url:
		new  = expanded_url
		old = ""
		do = is_short_url(new)
		count = 0
		while do == True and count<5:
			count = count + 1
			old = new
			try:
				new = urlShort2Long(new).encode('ascii','ignore')
				if new.find("http") == -1:
					tmp = old.split("/")
					new = "http://" + tmp[2] + new
				#pt("%s -> %s" % (old, new))
				do = is_short_url(new)
			except:
				#pt("wrong url...",True)
				#Tweets.objects.filter(expanded_url__icontains=expanded_url).delete()
				#new = None
				do = False
		url = new
	return url
					
def do_twitter_expanded_url(url):
	# url = none. do all
	# url = url, only on perticular url
	# url = "INCLUDE", all include url
	if url == None:
		ob = Tweets.objects.filter(expanded_url__isnull=True)
	elif url == "INCLUDE":
		urls = ShortPattern.objects.filter(cat="INCLUDE",inuse=1)
		q_sum = Q()
		for u in urls:
			q_sum.add(Q(expanded_url__icontains=u.pattern),Q.OR)
		ob = Tweets.objects.filter(q_sum)
	else:
		ob = Tweets.objects.filter(expanded_url__icontains=url)
	count = ob.count()
	pt("Number of tweets without full url: %s" % count)
	u = {}
	for o in ob:
		count -= 1
		if o.expanded_url:
			if not o.expanded_url in u:
				url = twitter_expanded_url(o.expanded_url)
				u[o.expanded_url] = url
			else:
				url = u[o.expanded_url]
			if url != o.expanded_url: # incase of value = none
				pt("%s left" % count)
				o.expanded_url = url
				o.save()
	# write comment
	d = {}
	d["app"] = "twitter_url"
	d["description"] = "Expand short tweet urls"
	write_cron_job_log(d)

def search_tweets(url,since_id, until):
	# url, the cleaned url,no www, no http
	# since_id, the id to search start
	# until xxxx-xx-xx date until
	#result = twitter.search(q=url,result_type="mixed",count=100)
	pt("searching %s, since:%s, until:%s" % (url,since_id,until),True)
	# tweepy
	api = tweepy.API(twitter)
	if since_id:
		result = tweepy.Cursor(api.search,q=url,result_type="mixed",since_id=since_id,count=100)
	elif until:
		result = tweepy.Cursor(api.search,q=url,result_type="mixed",until=until,count=100)
	else:
		result = tweepy.Cursor(api.search,q=url,result_type="mixed",count=100)
	return result.items()

def switch_twitter():
	global i
	global twitter
	find = False
	if i >= len(KEY):
		i = 0
	twitter = twitter_login(i)
	while find == False:
		print "checking accout %s..." % i
		SearchResult = search_tweets("test",None,None)
		try:
			for s in SearchResult:
				find = True
				print "successed!"
				break
		except Exception as inst:
			print inst.args
			print "failed! sleep 1 minute..."
			time.sleep(60)
			i += 1
			if i >= len(KEY):
				i = 0
			twitter = twitter_login(i)

def parse_tweets(s,ex):
	# s, dict
	# ex, array of exclude urls
	d = {}
	tid =None
	do = True
	# tweepy
	created_at = s.created_at.replace(tzinfo=utc)
	d["tweet_id"] = s.id
	tid = d["tweet_id"]
	if len(s.entities["urls"]) > 0:
		d["text"] = str(s.text.encode('ascii','ignore'))
		d["retweet_count"] = s.retweet_count
		d["created_at"] = created_at
		d["user_id"] = s.user.id
		d["profile_image_url_https"] = s.user.profile_image_url_https
		d["profile_image_url"] = s.user.profile_image_url
		d["user_screen_name"] = s.user.screen_name
		d["user_name"] = s.user.name
		d["user_url"] = s.user.url
		d["article_url"] = s.entities["urls"][0]["url"]
		d["expanded_url"] = twitter_expanded_url(s.entities["urls"][0]["expanded_url"])
		d["display_url"] = s.entities["urls"][0]["display_url"]
		if "media" in s.entities:
			d["media_url_https"] = s.entities["media"][0]["media_url_https"]
		# write into the database
		#Tweets.objects.filter(tweet_id = s["id"]).update(**d)
		# ignore the tweets
		for i in ex:
			if d["expanded_url"].find(i) != -1:
				do = False
		if d["expanded_url"] == None:
			do = False
	return (d,do) # for since id
	
def clean_url(url):
	url = url.lower()
	url = url.replace("http://","")
	url = url.replace("https://","")
	url = url.replace("www.","")
	return url

def url2doi(url):
	doi = None
	url = urlShort2Long(url)
	if url:
		if url.find("ncbi.nlm.nih.gov") != -1:
			if url.find("PMC") != -1:
				tmp = url.split("/")
				for t in tmp:
					if t.find("PMC") != -1:
						doi = t
			else:
				doi="pubmed"
		elif url.find(".pdf") != -1:
			doi = None # not sure how to do that
		else:
			# read from content
			try:
				r = requests.get(url)
				soup = BeautifulSoup(r.content)
				if soup.find("meta", {"name":"citation_doi"}) != None:
					doi = soup.find("meta", {"name":"citation_doi"})['content']
				elif soup.find("meta", {"scheme":"doi"}) != None:
					doi = soup.find("meta", {"scheme":"doi"})['content']
				else:
					#print url
					#sys.exit("check this url!")
					a = 1
				# We can add "doi pattern" here later
			except:
				a = 1
			if doi:
				doi=doi.replace("doi:","")
	return doi

def download_tweets():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "twitter_download"
	global i
	global twitter
	# load the tweet id
	td = datetime.datetime.now()
	dt1m = td - relativedelta(months=1) ###Download last 1 month
       #dt1m=td-relativedelta(days = 7) ###Download last 7 days
	pt("loading tweets id...")
	tw = Tweets.objects.filter(created_at__gte=dt1m)
	#tw = Tweets.objects.all()
	print tw.count()
	intw = {}
	for t in tw:
		intw[str(t.tweet_id)] = 1
	print len(intw)
	del tw
	# find the unique journal urls
	js = list(Journals.objects.values("url").distinct())
	js.append({'url':'ncbi.nlm.nih.gov/pubmed/'})
	js.append({'url':'arxiv.org'})
	js.append({'url':'biorxiv.org'})
	js.append({'url':'scireader.org'})
	pt("find the exclude url texts")
	excludes = URLExcludes.objects.all()
	ex = []
	for e in excludes:
		ex.append(str(e.text))
	del excludes
	# find a right twitter account
	pt("checking the right twitter account...")
	switch_twitter()
	total_tweets = 0
	j_order = 0
	j_count = len(js)
	for j in js:
		j_order = j_order + 1
		if j["url"]:
			url = clean_url(j["url"])
			pt("Processing %s (%i/%i)..." % (url,j_order,j_count))
			# parse the tweets
			since = None
			if SearchLog.objects.filter(url=url).count()>0:
				since = SearchLog.objects.get(url=url).since_id
			done = False
			until = None
			dblist = []
			while done == False:
				# start searching an url
				SearchResult = search_tweets(url,since,until)
				try:
					for s in SearchResult:
						#parse the data
						if not str(s.id) in intw:
							(d,do) = parse_tweets(s,ex)
							if do:
								if d["tweet_id"]:
									dblist.append(d)
									intw[str(d["tweet_id"])] = 1
									if not since:
										since = d["tweet_id"]
									else:
										if int(d["tweet_id"]) > int(since):
											since = d["tweet_id"]
								if len(dblist) == 1000:
									pt("Save 1000 data for %s..." % url)
									total_tweets = total_tweets + len(dblist)
									try:
									    writeIntoDBBulk(Tweets,dblist)
									except:
									    pt("in the single insert loop")
									    writeIntoDBSingleInsert(Tweets,dblist)
									dblist = []
				except Exception as inst: # exceed the url limit
					#print inst.args
					if str(inst.args).find("Rate limit exceeded") != -1:
						print "Rate limit exceeded!"
						i = i+1
						switch_twitter()
					else:
						done = True
				done = True
			if (len(dblist)) > 0:
				pt("Save %i data for %s..." % (len(dblist),url))
				try:
				    writeIntoDBBulk(Tweets,dblist)
				except:
				    writeIntoDBSingleInsert(Tweets,dblist)  
			# write comment
			total_tweets = total_tweets + len(dblist)
			# rewrite the log
			d = {}
			d["url"] = url
			d["SearchDate"] = datetime.datetime.utcnow().replace(tzinfo=utc)
			d["since_id"] = since
			if SearchLog.objects.filter(url=url).count()>0:
				SearchLog.objects.filter(url=url).update(**d)
			else:
				writeIntoDB(SearchLog,d)
			# count the numbers
			twitter_summary_one(url)
	dlog["description"] = "<b>%s</b> tweets were downloaded in past one day" % total_tweets
	dlog["count"] = total_tweets
	write_cron_job_log(dlog)
	del intw

def twitter_summary_one(url):
	#pt(url)
	# count the numbers
	d = {}
	d["url"] = url
	d["tweets"] = Tweets.objects.filter(expanded_url__icontains=url).count()
	d["linked"] = Tweets.objects.filter(expanded_url__icontains=url,dbid__isnull=False).count()
	if TweetsSummary.objects.filter(url=url).count() > 0:
		TweetsSummary.objects.filter(url=url).update(**d)
	else:
		writeIntoDB(TweetsSummary,d)
		
def twitter_summary_count():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "twitter_summary_count"
	js = list(Journals.objects.values("url").distinct())
	js.append({'url':'ncbi.nlm.nih.gov/pubmed/'})
	js.append({'url':'arxiv.org'})
	js.append({'url':'biorxiv.org'})
	for j in js:
		if j["url"]:
			url = clean_url(j["url"])
			twitter_summary_one(url)
	dlog["description"] = "Summarized the number of twitters for each journal"
	write_cron_job_log(dlog)
				
"""def twitter_doi():
	# find the known dois
	print "reading the old doi..."
	tw = Tweets.objects.filter(doi__isnull = False)
	doilist = {}
	searchlist = {} # search history
	for t in tw:
		doilist[t.article_url] = t.doi
	# read the first 1 month tweets
	n = datetime.datetime.utcnow().replace(tzinfo=utc)
	n = n - relativedelta(months=1)
	tweets = Tweets.objects.filter(doi__isnull = True,created_at__gte=n.strftime('%Y-%m-%d')).order_by("-created_at")
	count = tweets.count()
	for t in tweets:
		if not t.article_url in searchlist:
			print "%s (%s left)" % (t.article_url,count)
			count -= 1
			if t.article_url in doilist:
				doi = doilist[t.article_url]
			else:
				doi = url2doi(t.article_url)
			searchlist[t.article_url] = 1
			if doi:
				doilist[t.article_url] = doi
				t.doi = doi
				t.save()"""

def url_transform(url):
	urls = []
	urls.append(url)
	# split urls
	patterns = URLTransform.objects.filter(cat="SPLIT")
	for u in urls:
		for p in patterns:
			tmp = u.split(p.str1)
			if not tmp[int(p.str2)] in urls:
				urls.append(tmp[int(p.str2)])
	# replace the string
	patterns = URLTransform.objects.filter(cat="REPLACE")
	for p in patterns:
		us = []
		for u in urls:
			if p.str1 != "": # replace
				r = ""
				if p.str2:
					r = p.str2
				tmp = u.replace(p.str1,r)
				if not tmp in urls:
					urls.append(tmp)
			else:
				tmp = u + p.str2
				if not tmp in urls:
					us.append(tmp)
		if len(us) > 0:
			urls = urls + us
	# last, last segment
	patterns = URLTransform.objects.filter(cat="LAST")
	for p in patterns:
		for u in urls:
			if u.find(p.str1) != -1:
				if u.find("/") != -1:
					tmp = u.split("/")
					if not tmp[len(tmp)-1] in urls:
						urls.append(tmp[len(tmp)-1])
	# doi, everything after the doi mark
	patterns = URLTransform.objects.filter(cat="DOI")
	for p in patterns:
		for u in urls:
			if u.find(p.str1) != -1:
				tmp = u.split(p.str1)
				if not tmp[len(tmp)-1] in urls:
					urls.append(tmp[len(tmp)-1])
	return urls

def twitter_test():
	import gc
	pt("array testing...","blue")
	foo = ['bar' for _ in xrange(10000000)]
	pt("del foo")
	del foo
	pt("after del")
	
	pt("hash testing...","blue")
	foo = {}
	for i in xrange(10000000):
		foo["a_%i" % i] = "a_%i" % i
	pt("del foo")
	del foo
	pt("after del")
	
	
	url = "nature.com"
	td = datetime.datetime.utcnow().replace(tzinfo=utc)
	dt1m = td - relativedelta(months = 1)
	ts = Tweets.objects.filter(dbid__isnull=True,expanded_url__isnull=False,expanded_url__icontains=url, created_at__gte=dt1m)
	count = ts.count()
	pt("No. of Tweets not linked: %s" % count)
	
	urls = {}
	#pt("line: %s len: %i" % ("485",len(gc.get_referrers(urls))))
	#if match == "url":
	pt("load the pmid for %s..." % url)
	#ob = ""
	ob = PaperUrl.objects.filter(url__icontains=url).order_by("-dtstamp")[:40000]
	for o in ob:
		try:
			urls["a_%i" % o.id] = "a_%i" % o.id
			#for i in xrange(1000):
			#	urls["b_%i_%i" % (i,o.id)] = "b_%i_%i" % (i,o.PMID_id)
			urls["%s" % o.url] = int(o.PMID_id)
			#tmp = o.url.split("/")
			#urls[tmp[len(tmp)-1].lower()] = str(o.PMID.id)
		except:
			a = 1
	#pt("line: %s len: %i" % ("497",len(gc.get_referrers(urls))))
	pt("before del urls")
	del urls
	pt("after del urls")
	del ob
	pt("after del ob")
	del ts
	pt("after del ts")
	

def twitter_pmid_pair(j,dt1m):
	if j["url"] and j["url"] != "None" and j["url"] != "arxiv.org" and j["url"] != "biorxiv.org":
		url = clean_url(j["url"])
		pt("checking the tweets for %s..." % url,"blue")
		#ts = ""
		ts = Tweets.objects.filter(dbid__isnull=True,expanded_url__isnull=False,expanded_url__icontains=url, created_at__gte=dt1m)
		count = ts.count()
		pt("No. of Tweets not linked: %s" % count)
		if count > 0:
			match = 'url'
			pt("checking which method to match (doi/url/pmid) for %s..." % url)
			urls = {}
			if url.find("pubmed") != -1 or url.find("scireader") != -1:
				match = 'pmid'
			else:
				t = ts[0]
				patterns = URLTransform.objects.filter(cat="DOI")
				for p in patterns:
					if urllib.unquote(t.expanded_url).find(p.str1) != -1:
						match = "doi"
				pt("load the pmid for %s..." % url)
				#ob = ""
				ob = PaperUrl.objects.filter(url__icontains=url).order_by("-dtstamp")[:40000]
				for o in ob:
					try:
						urls[o.url] = str(o.PMID_id)
						tmp = o.url.split("/")
						urls[tmp[len(tmp)-1].lower()] = str(o.PMID_id)
					except:
						a = 1
				pt("load the doi for %s..." % url)
				#jous = ""
				jous = Journals.objects.filter(url__icontains=url)
				q_sum = Q()
				for jou in jous:
					#pt("%s..." % j.JournalTitle)
					q_sum.add(Q(JournalTitle=jou.JournalTitle,DateCreated__gte=dt1m),Q.OR)
				ob = Pubmed.objects.filter(q_sum)
				for o in ob:
					if o.DOI:
						if o.DOI != "NULL":
							urls[o.DOI] = str(o.id)
			pt("adding pmid...")
			for t in ts:
				count -= 1
				pmid = None
				u = urllib.unquote(t.expanded_url).lower()
				if u.find('%') != -1:
					u = urllib.unquote(u)
				if match == "pmid":
					try:
						pmid = re.search("pubmed/\d+",u).group(0).replace("pubmed/","")
					except:
						pmid = None
					if pmid == None:
						try:
							pmid = re.search("scireader.org/s/p/\d+",u).group(0).replace("scireader.org/s/p/","")
						except:
							pmid = None
					if pmid:
						pmid = pmid.replace('/','')
						
				else:
					if u in urls:
						pmid = urls[u]
					else:
						urlarray = url_transform(u)
						for u in urlarray:
							if u in urls:
								pmid = urls[u]
				if pmid:
					pt("%s -> %s (%s left)" % (t.expanded_url, pmid, count))
					t.dbid = pmid
					t.dbname="pubmed"
					t.dtlinked = datetime.datetime.utcnow().replace(tzinfo=utc)
					t.save()
			# count the numbers
			twitter_summary_one(url)
			del urls

def twitter_pmid(url):
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "twitter_pmid"
	# load the pmid and url pair
	# url = debug_url
	td = datetime.datetime.utcnow().replace(tzinfo=utc)
	dt1m = td - relativedelta(months = 1)
	if url:
		js = []
		js.append({'url':url})
	else:
		js = list(TweetsSummary.objects.filter(tweets__gt=1000).values("url").distinct())
		js.append({'url':'ncbi.nlm.nih.gov/pubmed/'})
		js.append({'url':'scireader.org/s/p/'})
	for j in js:
		twitter_pmid_pair(j,dt1m)
	# write comment
	dlog["description"] = "Connected tweets with pmids"
	write_cron_job_log(dlog)

def check_more_tweets(page,dbname,dbid):
	more = True
	if Tweets.objects.all().count() > 0:
		if Tweets.objects.filter(dbname=dbname,dbid=dbid).count() <= TWEETS_PER_PAGE * int(page):
			more = False
	else:
		more = False
	return more

def collect_top_tweets_papers():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "twitter_papers"
	pt("Cleaning the papers...")
	TweetsPapers.objects.all().delete()
	pt("Collecting the papers twittered in last 2 days...")
	d = 2
	ts = Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')).order_by('-count')[:100]
	dblist = []
	for t in ts:
		data = {}
		data["dbname"] = "pubmed"
		data["dbid"] = t["dbid"]
		data["count"] = t["count"]
		dblist.append(data)
	writeIntoDBBulk(TweetsPapers,dblist)
	pt("Collecting the papers twittered in last 10 days...")
	d = 10
	ts = Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')).order_by('-count')[:1000]
	dblist = []
	for t in ts:
		if TweetsPapers.objects.filter(dbname="pubmed",dbid=t["dbid"]).count() == 0:
			data = {}
			data["dbname"] = "pubmed"
			data["dbid"] = t["dbid"]
			data["count"] = t["count"]
		dblist.append(data)
	writeIntoDBBulk(TweetsPapers,dblist)
	if TweetsPapers.objects.all().count() == 0:
		pt("Collecting the papers twittered in last 1 year ...")
		d = 365
		dblist = []
		ts = Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')).order_by('-count')[:100]
		for t in ts:
			data = {}
			data["dbname"] = "pubmed"
			data["dbid"] = t["dbid"]
			data["count"] = t["count"]
			dblist.append(data)
		writeIntoDBBulk(TweetsPapers,dblist)
	dlog["description"] = "Collected top twittered papers"
	write_cron_job_log(dlog)

def collect_twitter_accounts():
	TweetAccounts.objects.all().delete()
	TweetPMIDs.objects.all().delete()
	file = "%s/rec/topics/Current_basic_dataset_with_IF_and_LDA.csv" % settings.BASE_DIR
	cutoff = 0.1
	delim = ","
	print "Cutoff: %s" % cutoff
	pt("Collecting exist topics...")
	topics_db = []
	for s in Subcategory.objects.all():
		topics_db.append(s.id)
	pt("Collecting twitter pmid user pairs...")
	pmid2user = {}
	for t in Tweets.objects.filter(dbid__isnull=False):
		pmid2user[str(t.dbid)] = t.user_id
	topics = {} # to store different topics
	users = {} # topic, user_id
	pmids = {} # topic - > pmids
	fh = open(file,'r')
	lines = 375747
	dbarray = []
	for l in fh:
		parts = l.split(delim)
		lines = lines -1
		if parts[0] != "pmid":
			pt("Processing %s, %s left..." % (parts[0],lines))
			topic_line = []
			prob = [2,4,6]
			for p in prob:
				if float(parts[p]) >= cutoff:
					topic_line.append(parts[p-1])
					topics[parts[p-1]] = 1
					if not parts[p-1] in pmids:
						pmids[parts[p-1]] = []
					pmids[parts[p-1]].append(parts[0])
			if len(topic_line) > 0:# at least one topic was selected
				us = []
				if Tweets.objects.filter(dbid=parts[0]).count() > 0:# the paper has tweets
					for t in Tweets.objects.filter(dbid=parts[0]):
						if not t.user_id in us:
							us.append(t.user_id)
				# added users to topics
				for t in topic_line:
					if not t in users:
						users[str(t)] = {}
					for u in us:
						users[str(t)][str(u)] = 1
					s = int(t) + 1
					if s in topics_db:
						d = {}
						d["subcat_id"] = s
						d["pmid"] = parts[0]
						if str(parts[0]) in pmid2user:
							d["user_id"] = pmid2user[str(parts[0])]
							dbarray.append(d)
						if len(dbarray) > 1000:
							pt("Writing 1000...","green")
							writeIntoDBBulk(TweetPMIDs,dbarray)
							dbarray = []
		#if lines == 374000:
		#	break
	if len(dbarray) > 0:
		writeIntoDBBulk(TweetPMIDs,dbarray)
	# collect the tweets for each user
	tweets = {} # number of tweets for each user
	pt("Collecting twitter count...")
	for c in Tweets.objects.values('user_id').annotate(count=Count('user_id')):
		tweets[str(c["user_id"])] = c["count"]
	pt("Collecting subcat count...")
	subcount = {}
	for c in TweetPMIDs.objects.values('user_id','subcat_id').annotate(count=Count('user_id')):
		if not str(c["user_id"]) in subcount:
			subcount[str(c["user_id"])] = {}
		subcount[str(c["user_id"])][str(c["subcat_id"])] = c["count"]
	dbarray = []
	for t in topics:
		s = int(t) + 1
		if s in topics_db:#make sure the topic is in the db
			pt("Working on topic %s..." % s)
			for u in users[str(t)]:
				d = {}
				d["subcat_id"] = int(s)
				d["user_id"] = u
				d["count"] = tweets[str(u)]
				try:
					d["subcount"] = subcount[str(u)][str(s)]
				except:
					d["subcount"] = 0
				#d["subcount"] = Tweets.objects.filter(user_id=u,dbid__in=pmids[t]).count() # make sure hte pmids t is = t-1
				dbarray.append(d)
				if len(dbarray) > 1000:
					pt("Writing 1000...","green")
					writeIntoDBBulk(TweetAccounts,dbarray)
					dbarray = []
	if len(dbarray) > 0:
		writeIntoDBBulk(TweetAccounts,dbarray)
	
def filter_twitter_accounts():
	# cutoffs for selecting twitter users
	cutoff_subcat = 5
	cutoff_super = 10
	TweetAccountsSelected.objects.all().delete()
	pt("Collecting subcat users...")
	users_subcat = {}
	for p in TweetAccountsPlot.objects.filter(plot='subcat_on_user_tweets',y__gte=cutoff_subcat):
		users_subcat[p.x] = p.z
	pt("Collecting super topic users...")
	users_super = {}
	for p in TweetAccountsPlot.objects.filter(plot='supertopic_on_user_tweets',y__gte=cutoff_super):
		users_super[p.x] = 1
	pt("Filtering the users...")
	selectarray = []
	total = len(users_subcat)
	for u in users_subcat:
		total = total - 1
		pt("%s left, processing %s ..." % (total,u))
		if u in users_super:
			d = {}
			d["user_id"] = u
			d["subcat_id"] = users_subcat[u]
			selectarray.append(d)
			if len(selectarray) >1000:
				pt("Writing 1000 records into db...","green")
				writeIntoDBBulk(TweetAccountsSelected,selectarray)
				selectarray = []
	if len(selectarray) > 0:
		writeIntoDBBulk(TweetAccountsSelected,selectarray)
	# check the result
	minusers = 10
	pt("Checking number of users selected for each topic...")
	f = 0
	for c in Subcategory.objects.all():
		if TweetAccountsSelected.objects.filter(subcat_id=c.id).count() < minusers:
			f = f + 1
	pt("%s topics have less than %s users." % (f,minusers),'red')
	# check number of tweets in last two days
	mintweets = 10
	days = 2
	pt("Checking number of tweets in last %s days for each topic..." % days)
	f = 0
	for c in Subcategory.objects.all():
		td = datetime.datetime.now().replace(tzinfo=utc)
		dt_limit = td - relativedelta(days = days)
		users = []
		for u in TweetAccountsSelected.objects.filter(subcat_id=c.id):
			users.append(u.user_id)
		if Tweets.objects.filter(created_at__gte=dt_limit,user_id__in=users).count() < mintweets:
			f = f + 1
	pt("%s topics have less than %s tweets in last %s days." % (f,mintweets,days),'red')
	
	
################################################################################
def view(request,dbname,edbid,head):
	add_visiting_record(request)
	data={}
	dbid = eid2id(edbid)
	data["dbname"] = dbname
	data["dbid"] = dbid
	data["edbid"] = edbid
	data["data_target"] = "[role='twitter_content'][edbid='"+edbid+"']"
	if head == "yes":
		data["total"] = Tweets.objects.filter(dbname=dbname,dbid=dbid).count()
		#data["total"] = len(Tweets.objects.filter(dbname=dbname,dbid=dbid).values_list('text', flat=True).distinct())
	data["app"] = "twitter"
	data["head"] = head
	return render_to_response('twitter/view.html',data,context_instance=RequestContext(request))

def content(request,dbname,edbid,batch,page):
	add_visiting_record(request)
	data={}
	dbid = eid2id(edbid)
	data["page"] = page
	data["perpage"] = 10
	if dbname == "new": # for the new tweets
		topicids = []
		if request.user.is_authenticated():
			if MyTopics.objects.filter(userid=request.user.id).count() > 0:
				for m in MyTopics.objects.filter(userid=request.user.id):
					topicids.append(m.subcatid.id)
		if len(topicids) == 0:
			taccount = TweetAccountsSelected.objects.all()[:500]
		else:
			taccount = TweetAccountsSelected.objects.filter(subcat_id__in=topicids)
		tarray = []
		for a in taccount:
			tarray.append(a.user_id)
		#data["allitems"] = Tweets.objects.filter(user_id__in=tarray).order_by("-created_at")
		data["total"] = Tweets.objects.filter(user_id__in=tarray).count()
		data["perpage"] = 20
		batch = "more"
	else:
		data["total"] = Tweets.objects.filter(dbname=dbname,dbid=dbid).exclude(text__contains='RT @').count()
	#uniq = Tweets.objects.filter(dbname=dbname,dbid=dbid).values_list('text', flat=True).distinct()
	#data["total"] = len(uniq)
	data["start"] = (int(page)-1)*data["perpage"] +1
	if batch == "more":
		data["end"] = data["start"] + data["perpage"] -1
	else:
		data["end"] = data["total"]
	data["more"] = False
	if data["end"] < data["total"]:
		data["more"] = reverse("twitter_content",args=[dbname,edbid,"more",int(data["page"]) + 1,])
		data["all"] = reverse("twitter_content",args=[dbname,edbid,"all",int(data["page"]) + 1,])
	else:
		data["end"] = data["total"]
	#data["tweets"] = Tweets.objects.filter(dbname=dbname,dbid=dbid,text__in=uniq).order_by("-created_at")[data["start"]-1:data["end"]]
	if dbname == "new":
		data["tweets"] = Tweets.objects.filter(user_id__in=tarray).order_by("-created_at")[data["start"]-1:data["end"]]
	else:
		data["tweets"] = Tweets.objects.filter(dbname=dbname,dbid=dbid).exclude(text__contains='RT @').order_by("-created_at")[data["start"]-1:data["end"]]
	data["edbid"] = edbid
	data["app"] = "twitter"
	return render_to_response('twitter/content.html',data,context_instance=RequestContext(request))

################################################################################
# recent tweets

def recent(request):
	add_visiting_record(request)
	data={}
	d = 2
	#data["total"] = len(Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')))
	#data["total"] = TweetsPapers.objects.all().count()
	if request.user.is_authenticated():
		data["total"] = Recommendations.objects.filter(lid=-2,userid=request.user.id).count()
		if data["total"] == 0:
			data["total"] = Recommendations.objects.filter(lid=-2,userid__isnull=True).count()
	else:
		data["total"] = Recommendations.objects.filter(lid=-2,userid__isnull=True).count()
	data["edbid"] = id2eid(d)
	data["app"] = "twitter_recent"
	data["data_target"] = "#accordion"
	return render_to_response('twitter/recent.html',data,context_instance=RequestContext(request))
	
def recent_load(request,edbid,batch,page):
	add_visiting_record(request)
	data={}
	d = eid2id(edbid)
	data["page"] = page
	data["perpage"] = 20
	#data["total"] = len(Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')))
	#data["total"] = TweetsPapers.objects.all().count()
	if request.user.is_authenticated():
		data["total"] = Recommendations.objects.filter(lid=-2,userid=request.user.id).count()
		if data["total"] == 0:
			data["total"] = Recommendations.objects.filter(lid=-2,userid=None).count()
	else:
		data["total"] = Recommendations.objects.filter(lid=-2,userid=None).count()
	data["start"] = (int(page)-1)*data["perpage"] +1
	if batch == "more":
		data["end"] = data["start"] + data["perpage"] -1
	else:
		data["end"] = data["total"]
	data["more"] = False
	if data["end"] < data["total"]:
		data["more"] = reverse("twitter_recent_load",args=[edbid,"more",int(data["page"]) + 1])
		data["all"] = reverse("twitter_recent_load",args=[edbid,"all",int(data["page"]) + 1])
	else:
		data["end"] = data["total"]
	#data["tweets"] = Tweets.objects.filter(dbname='pubmed',created_at__gte=datetime.date.today()- relativedelta(days=d)).values('dbid').annotate(count=Count('dbid')).order_by('-count')[data["start"]-1:data["end"]]
	#data["tweets"] = TweetsPapers.objects.all()[data["start"]-1:data["end"]]
	if request.user.is_authenticated():
		if Recommendations.objects.filter(lid=-2,userid=request.user.id).count() > 0:
			data["tweets"] = Recommendations.objects.filter(lid=-2,userid=request.user.id)[data["start"]-1:data["end"]]
		else:
			data["tweets"] = Recommendations.objects.filter(lid=-2,userid=None)[data["start"]-1:data["end"]]
	else:
		data["tweets"] = Recommendations.objects.filter(lid=-2,userid=None)[data["start"]-1:data["end"]]
	# download papers
	pmidarray = []
	for t in data["tweets"]:
		pmidarray.append(t.dbid)
	from pubmed.views import downloadPMIDs
	downloadPMIDs(pmidarray)
	data["edbid"] = edbid
	data["app"] = "twitter_recent"
	return render_to_response('twitter/recent_list.html',data,context_instance=RequestContext(request))

@staff_member_required
def home(request):
	data = {}
	data["tlr_sb"] = "twitter/sidebar.html"
	data["tlr_content"] = 'twitter/home.html'
	template = 'base_lr.html'
	return render_to_response(template, data, context_instance=RequestContext(request))

@staff_member_required
def summary(request, view):
	data={}
	if view == "lr":
		data["tlr_sb"] = "twitter/sidebar.html"
		data["tlr_content"] = 'twitter/summary.html'
		template = 'base_lr.html'
	else:
		template = 'twitter/summary.html'
	db = {}
	from pubmed.views import getPubmedTables
	from pubmed.models import PaperUrl
	db["dblist"] = getPubmedTables()
	data["tweets_total"] = Tweets.objects.all().count()
	data["tweets_pmid"] =  Tweets.objects.filter(dbid__isnull=False).count()
	data["tweets_p_pmid"] = (float(data["tweets_pmid"])/int(data["tweets_total"])) * 100
	data["pmid_total"] = db["dblist"]["Pubmed"].objects.all().count()
	data["doi_total"] = db["dblist"]["Pubmed"].objects.filter(DOI__isnull=False).count()
	data["doi_total_p"] = float(data["doi_total"]) / data["pmid_total"] * 100
	data["doi_checked"] = PaperUrl.objects.all().count()
	data["doi_checked_p"] = float(data["doi_checked"]) / data["doi_total"] * 100
	data["doi_url"] = PaperUrl.objects.filter(url__isnull=False).count()
	data["doi_url_p"] = float(data["doi_url"]) / data["doi_checked"] * 100
	data["doi_pmid"] = Tweets.objects.values("dbid").distinct().count()
	data["doi_pmid_p"] = float(data["doi_pmid"]) / data["doi_url"] * 100
	return render_to_response(template, data, context_instance=RequestContext(request))

@staff_member_required
def debug(request):
	data={}
	url = get_debug_url()
	n = datetime.datetime.now()
	d = n - relativedelta(days=5)
	q_sum = Q()
	excludes = URLExcludes.objects.all()
	for e in excludes:
		q_sum.add(Q(expanded_url__icontains=e.text),Q.OR)
	data["tweets"] = Tweets.objects.filter(expanded_url__icontains=url,dbid__isnull=True,created_at__lt=d).exclude(q_sum).order_by("-created_at")[:50]
	data["total"] = Tweets.objects.filter(expanded_url__icontains=url,dbid__isnull=True,created_at__lt=d).exclude(q_sum).count()
	data["url"] = url
	return render_to_response('twitter/debug.html', data, context_instance=RequestContext(request))

@staff_member_required
def debug_search(request,term):
	from pubmed.models import Pubmed
	data={}
	data["papers"] = Pubmed.objects.filter(ArticleTitle__icontains=term)[:5]
	return render_to_response('twitter/debug_search.html', data, context_instance=RequestContext(request))

@staff_member_required
def linkpaper(request,etid):
	tid = eid2id(etid)
	data = {}
	data["tweet"] = Tweets.objects.get(id=tid)
	data["title"] = "Add a paper by PMID"
	data["form_action"] = reverse("twitter_debug_linkpaper",args=[etid])
	if request.method == 'POST':
		data["form"] = LinkPaperForm(data["tweet"],request.POST)
		if data["form"].is_valid():
			data["form"].save()
			return HttpResponseRedirect(reverse('backend_home'))
		else:
			data["tlr_content"] = 'base_form.html'
			return render_to_response('base_tlr.html', data, context_instance=RequestContext(request))
	else:
		data["form"] =LinkPaperForm(data["tweet"])
	return render_to_response('base_modal_form.html', data, context_instance=RequestContext(request))


@staff_member_required
def exclude(request):
	data={}
	return render_to_response('twitter/exclude.html', data, context_instance=RequestContext(request))

@staff_member_required
def exclude_load(request):
	data={}
	data["excludes"] = URLExcludes.objects.all()
	return render_to_response('twitter/exclude_load.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_exclude_edit(request,eeid):
	eid = eid2id(eeid)
	data = {}
	if int(eid) != 0:
		data["ex"] = URLExcludes.objects.get(id=lid)
		data["title"] = "Edit URL exclude"
	else:
		data["title"] = "Add a URL exclude"
	data["form_action"] = reverse("twitter_exclude_edit",args=[eeid])
	if request.method == 'POST':
		data["form"] = URLExcludeForm(request.POST)
		if data["form"].is_valid():
			data["form"].save({"id":eid})
			return HttpResponseRedirect(reverse('backend_home'))
		else:
			data["tlr_content"] = 'base_form.html'
			return render_to_response('base_tlr.html', data, context_instance=RequestContext(request))
	else:
		if int(eid) != 0:
			arg = model_to_dict(data["ex"], fields=[], exclude=[])
			data["form"] =URLExcludeForm(arg)
		else:
			data["form"] =URLExcludeForm()
	return render_to_response('base_modal_form.html', data, context_instance=RequestContext(request))

@staff_member_required
def shorturl(request):
	data={}
	data["patterns"] = ShortPattern.objects.all().order_by("cat")
	return render_to_response('twitter/shorturl.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_shorturl_edit(request,esid):
	sid = eid2id(esid)
	data = {}
	if int(sid) != 0:
		data["pattern"] = ShortPattern.objects.get(id=sid)
		data["title"] = "Edit URL short pattern"
	else:
		data["title"] = "Add a URL short pattern"
	data["form_action"] = reverse("twitter_shorturl_edit",args=[esid])
	if request.method == 'POST':
		data["form"] = URLShortPatternForm(request.POST)
		if data["form"].is_valid():
			data["form"].save({"id":sid})
			return HttpResponseRedirect(reverse('backend_home'))
		else:
			data["tlr_content"] = 'base_form.html'
			return render_to_response('base_tlr.html', data, context_instance=RequestContext(request))
	else:
		if int(sid) != 0:
			arg = model_to_dict(data["pattern"], fields=[], exclude=[])
			data["form"] =URLShortPatternForm(arg)
		else:
			data["form"] =URLShortPatternForm()
	return render_to_response('base_modal_form.html', data, context_instance=RequestContext(request))

@staff_member_required
def urltransform(request):
	data={}
	data["transforms"] = URLTransform.objects.all().order_by("-cat","str1")
	return render_to_response('twitter/urltransform.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_urltransform_edit(request,euid):
	uid = eid2id(euid)
	data = {}
	if int(uid) != 0:
		data["transform"] = URLTransform.objects.get(id=sid)
		data["title"] = "Edit URL transform pattern"
	else:
		data["title"] = "Add a URL transform pattern"
	data["form_action"] = reverse("twitter_urltransform_edit",args=[euid])
	if request.method == 'POST':
		data["form"] = URLTransformForm(request.POST)
		if data["form"].is_valid():
			data["form"].save({"id":uid})
			return HttpResponseRedirect(reverse('backend_home'))
		else:
			data["tlr_content"] = 'base_form.html'
			return render_to_response('base_tlr.html', data, context_instance=RequestContext(request))
	else:
		if int(uid) != 0:
			arg = model_to_dict(data["transform"], fields=[], exclude=[])
			data["form"] =URLTransformForm(arg)
		else:
			data["form"] =URLTransformForm()
	return render_to_response('base_modal_form.html', data, context_instance=RequestContext(request))

@staff_member_required
def journal_count(request):
	data = {}
	data["counts"] = TweetsSummary.objects.all().order_by("-tweets")[:1000]
	return render_to_response('twitter/journal_count.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics(request):
	data = {}
	data["cats"] = Subcategory.objects.all()
	return render_to_response('backend/twitter_topics.html', data, context_instance=RequestContext(request))

def update_topics_per_user(plot):
	TweetAccountsPlot.objects.filter(plot=plot).delete()
	plotarray = []
	total = TweetAccounts.objects.values('user_id').annotate(count=Count('user_id')).count()
	for u in TweetAccounts.objects.values('user_id').annotate(count=Count('user_id')):
		total = total - 1
		pt("%s left..." % total)
		d = {}
		d["plot"] = plot
		d["x"] = u["user_id"]
		d["y"] = u["count"]
		plotarray.append(d)
		if len(plotarray) > 1000:
			pt("Writing data...",'green')
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
			plotarray = []
	if len(plotarray) > 0:
		writeIntoDBBulk(TweetAccountsPlot,plotarray)

@staff_member_required
def twitter_backend_topics_chart_topics_per_user(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["User","Topics"]
	data["chart_data"] = []
	plot = "topics_per_user"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		update_topics_per_user(plot)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y")[:250]:
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_topics_per_user_bin(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Topic Count","Bin Count"]
	data["chart_data"] = []
	plot = "topics_per_user_bin"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="topics_per_user").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="topics_per_user"):
				y = p.y
				if not str(y) in bin_tweets:
					bin_tweets[str(y)] = 0
				bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).extra(select={"x_int": "CONVERT(x, SIGNED)"}).order_by("x_int"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_tweets(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Topic","Count"]
	data["chart_data"] = []
	plot = "total_tweets_per_topic"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		plotarray = []
		cats = Subcategory.objects.all()
		for c in cats:
			ta = TweetAccounts.objects.filter(subcat_id=c.id)
			total = 0
			for t in ta:
				total = total + t.count
			d = {}
			d["plot"] = plot
			d["x"] = "%s (%s)" % (str(c.title),c.id)
			d["y"] = total
			plotarray.append(d)
		writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_users(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Topic","Count"]
	data["chart_data"] = []
	plot = "total_users_per_topic"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		plotarray = []
		cats = Subcategory.objects.all()
		for c in cats:
			d = {}
			d["plot"] = plot
			d["x"] = "%s (%s)" % (str(c.title),c.id)
			d["y"] = TweetAccounts.objects.filter(subcat_id=c.id).count()
			plotarray.append(d)
		writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

def update_tweets_per_user(plot):
	plot = "tweets_per_user"
	TweetAccountsPlot.objects.filter(plot=plot).delete()
	plotarray = []
	pt("Collecting the count...")
	for u in TweetAccounts.objects.values("user_id","count").distinct():
		d = {}
		d["plot"] = plot
		d["x"] = u['user_id']
		d["y"] = u['count']
		plotarray.append(d)
		if len(plotarray) > 1000:
			pt("Writing data...")
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
			plotarray = []
	if len(plotarray) > 0:
		writeIntoDBBulk(TweetAccountsPlot,plotarray)

@staff_member_required
def twitter_backend_topics_chart_active_users(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Users","Count"]
	data["chart_data"] = []
	plot = "tweets_per_user"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		update_tweets_per_user(plot)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y")[:250]:
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_active_users_bin(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Users","Count"]
	data["chart_data"] = []
	plot = "tweets_per_user_bin"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="tweets_per_user").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="tweets_per_user"):
				if p.y <= 50:
					y = p.y
					if not str(y) in bin_tweets:
						bin_tweets[str(y)] = 0
					bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_active_users_bin2(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Users","Count"]
	data["chart_data"] = []
	plot = "tweets_per_user_bin2"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="tweets_per_user").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="tweets_per_user"):
				if p.y <= 1000 and p.y > 50:
					y = int(float(p.y)/100 + 1) * 100
					if not str(y) in bin_tweets:
						bin_tweets[str(y)] = 0
					bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_active_users_bin3(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Users","Count"]
	data["chart_data"] = []
	plot = "tweets_per_user_bin3"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="tweets_per_user").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="tweets_per_user"):
				if p.y > 1000:
					y = int(float(p.y)/1000 + 1) * 1000
					if not str(y) in bin_tweets:
						bin_tweets[str(y)] = 0
					bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_active_users_cutoff(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Cutoff","Percentage (%)"]
	data["chart_data"] = []
	plot = "tweets_per_user_cutoff"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="tweets_per_user").count() > 0:
			cutoffs = [1,2,3,4,5,6,7,8,9,10,20,50,100,500,1000]
			plotarray = []
			total_users = TweetAccountsPlot.objects.filter(plot="tweets_per_user").count()
			for c in cutoffs:
				d = {}
				d["plot"] = plot
				d["x"] = c
				d["y"] = (float(TweetAccountsPlot.objects.filter(plot="tweets_per_user",y__gt=c).count())/total_users) * 100
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

def update_subcat_on_user_tweets(plot):
	TweetAccountsPlot.objects.filter(plot=plot).delete()
	cutoff = 10
	if TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff).count() > 0:
		plotarray = []
		total = TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff).count()
		for u in TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff):
			total = total - 1
			pt("%s left, processing %s..." % (total,u.x))
			d = {}
			d["plot"] = plot
			d["x"] = u.x
			a = TweetAccounts.objects.filter(user_id=u.x).order_by("-subcount")[:1][0]
			if a.count > 0:
				d["y"] = (float(a.subcount)/float(a.count)) * 100
				d["z"] = a.subcat.id
				plotarray.append(d)
			if len(plotarray) > 1000:
				pt("Writing data...",'green')
				writeIntoDBBulk(TweetAccountsPlot,plotarray)
				plotarray = []
		if len(plotarray) > 0:
			writeIntoDBBulk(TweetAccountsPlot,plotarray)

@staff_member_required
def twitter_backend_topics_chart_subcat_on_user_tweets(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Subcat","Percentage (%)"]
	data["chart_data"] = []
	plot = "subcat_on_user_tweets"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		update_subcat_on_user_tweets(plot)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y")[:250]:
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_subcat_on_user_tweets_bin(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Percentage","Count"]
	data["chart_data"] = []
	#data["chart_h_max"] = 100
	#data["chart_h_min"] = 0
	plot = "subcat_on_user_tweets_bin"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets"):
				y = int(p.y)
				if not str(y) in bin_tweets:
					bin_tweets[str(y)] = 0
				bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).extra(select={"x_int": "CONVERT(x, SIGNED)"}).order_by("x_int"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_subcat_on_user_tweets_cutoff(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Cutoff","Percentage (%)"]
	data["chart_data"] = []
	plot = "subcat_on_user_tweets_cutoff"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets").count() > 0:
			cutoffs = range(1,100)
			plotarray = []
			total_users = TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets").count()
			for c in cutoffs:
				d = {}
				d["plot"] = plot
				d["x"] = c
				d["y"] = (float(TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets",y__gt=c).count())/total_users) * 100
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).extra(select={"x_int": "CONVERT(x, SIGNED)"}).order_by("x_int"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

def update_supertopic_on_user_tweets(plot):
	TweetAccountsPlot.objects.filter(plot=plot).delete()
	cutoff = 10
	if TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff).count() > 0:
		plotarray = []
		total = TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff).count()
		# create super topic network
		pt("Creating the super topic network...")
		topic_ids = {}
		for s in Subjects.objects.all():
			subcats = []
			for c in Subcategory.objects.filter(subjectid_id=s.id):
				subcats.append(c.id)
			for c in Subcategory.objects.filter(subjectid_id=s.id):
				topic_ids[c.id] = subcats
		for u in TweetAccountsPlot.objects.filter(plot="tweets_per_user", y__gte=cutoff):
			total = total - 1
			pt("%s left, processing %s..." % (total,u.x))
			d = {}
			d["plot"] = plot
			d["x"] = u.x
			subcat = TweetAccounts.objects.filter(user_id=u.x).order_by("-subcount")[:1][0]
			topic_count = 0
			for t in TweetAccounts.objects.filter(user_id=u.x,subcat_id__in=topic_ids[subcat.subcat_id]):
				topic_count = topic_count + t.subcount
			d["y"] = (float(topic_count)/float(u.y)) * 100
			plotarray.append(d)
			if len(plotarray) > 1000:
				pt("Writing data...",'green')
				writeIntoDBBulk(TweetAccountsPlot,plotarray)
				plotarray = []
		if len(plotarray) > 0:
			writeIntoDBBulk(TweetAccountsPlot,plotarray)

@staff_member_required
def twitter_backend_topics_chart_supertopic_on_user_tweets(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Super Topic","Percentage (%)"]
	data["chart_data"] = []
	plot = "supertopic_on_user_tweets"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		update_supertopic_on_user_tweets(plot)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y")[:250]:
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_supertopic_on_user_tweets_bin(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Percentage","Count"]
	data["chart_data"] = []
	plot = "supertopic_on_user_tweets_bin"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets").count() > 0:
			plotarray = []
			bin_tweets = {}
			for p in TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets"):
				y = int(p.y)
				if not str(y) in bin_tweets:
					bin_tweets[str(y)] = 0
				bin_tweets[str(y)] = bin_tweets[str(y)] + 1
			for b in bin_tweets:
				d = {}
				d["plot"] = plot
				d["x"] = int(b)
				d["y"] = bin_tweets[b]
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).extra(select={"x_int": "CONVERT(x, SIGNED)"}).order_by("x_int"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_user_tweets_bin_comparison(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Percentage","Sub Topic","Super Topic"]
	data["chart_data"] = []
	data["chart_legend"] = True
	data["chart_h_max"] = 5000
	data["chart_h_min"] = 0
	# collect data for sub topic
	subcat = {}
	for p in TweetAccountsPlot.objects.filter(plot="subcat_on_user_tweets_bin"):
		subcat[str(p.x)] = p.y
	# collect data for super topic
	supercat = {}
	for p in TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets_bin"):
		supercat[p.x] = p.y
	for x in range(0,51):
		if not str(x) in subcat:
			subcat[str(x)] = 0
		if not str(x) in supercat:
			supercat[str(x)] = 0
		data["chart_data"].append([str(x),int(subcat[str(x)]),int(supercat[str(x)])])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_supertopic_on_user_tweets_cutoff(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Cutoff","Percentage (%)"]
	data["chart_data"] = []
	plot = "supertopic_on_user_tweets_cutoff"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets").count() > 0:
			cutoffs = range(1,100)
			plotarray = []
			total_users = TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets").count()
			for c in cutoffs:
				d = {}
				d["plot"] = plot
				d["x"] = c
				d["y"] = (float(TweetAccountsPlot.objects.filter(plot="supertopic_on_user_tweets",y__gt=c).count())/total_users) * 100
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).extra(select={"x_int": "CONVERT(x, SIGNED)"}).order_by("x_int"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_filtered_users(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Count","Topic"]
	data["chart_data"] = []
	plot = "filtered_users"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsSelected.objects.all().count() > 0:
			plotarray = []
			for c in Subcategory.objects.all():
				d = {}
				d["plot"] = plot
				d["x"] = "%s (%s)" % (str(c.title),c.id)
				d["y"] = TweetAccountsSelected.objects.filter(subcat_id=c.id).count()
				plotarray.append(d)
			writeIntoDBBulk(TweetAccountsPlot,plotarray)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

def update_filtered_tweets(plot):
	TweetAccountsPlot.objects.filter(plot=plot).delete()
	days = 2
	plotarray = []
	for c in Subcategory.objects.all():
		pt("Processing %s (%s)..." % (c.title,c.id))
		td = datetime.datetime.now().replace(tzinfo=utc)
		dt_limit = td - relativedelta(days = days)
		users = []
		for u in TweetAccountsSelected.objects.filter(subcat_id=c.id):
			users.append(u.user_id)
		d = {}
		d["plot"] = plot
		d["x"] = "%s (%s)" % (str(c.title),c.id)
		d["y"] = Tweets.objects.filter(created_at__gte=dt_limit,user_id__in=users).count()
		plotarray.append(d)
	writeIntoDBBulk(TweetAccountsPlot,plotarray)

@staff_member_required
def twitter_backend_topics_chart_filtered_tweets(request):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Count","Topic"]
	data["chart_data"] = []
	plot = "filtered_tweets"
	if TweetAccountsPlot.objects.filter(plot=plot).count() == 0:
		if TweetAccountsSelected.objects.all().count() > 0:
			update_filtered_tweets(plot)
	for p in TweetAccountsPlot.objects.filter(plot=plot).order_by("-y"):
		data["chart_data"].append([str(p.x),int(p.y)])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_percentage(request,tid):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["User","P"]
	#data["chart_v_min"] = 0
	#data["chart_v_max"] = 1
	data["chart_data"] = []
	users = TweetAccounts.objects.filter(subcat_id=tid,count__gte=2).order_by("p")
	for u in users:
		data["chart_data"].append(["%s" % str(u.user_id),float(u.p)*100])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))

@staff_member_required
def twitter_backend_topics_chart_cut(request,min,max):
	data = {}
	data["chart_width"] = "100%"
	data["chart_height"] = "350px"
	data["chart_labels"] = ["Topic","P"]
	data["chart_data"] = []
	cats = Subcategory.objects.all()
	for c in cats:
		ta = TweetAccounts.objects.filter(subcat_id=c.id)
		users = []
		for t in ta:
			users.append(t.user_id)
		total1 = Tweets.objects.filter(user_id__in=users).count()
		ta = TweetAccounts.objects.filter(subcat_id=c.id,p__gte=float(min),p__lte=float(max))
		users = []
		for t in ta:
			users.append(t.user_id)
		total2 = Tweets.objects.filter(user_id__in=users).count()
		if total1 != 0:
			p = float(total2)/float(total1)
		else:
			p = 0
		data["chart_data"].append(["%s (%s)" % (str(c.title),c.id),float(p)*100])
	return render_to_response('chart_bar.html', data, context_instance=RequestContext(request))
