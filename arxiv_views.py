from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from django.utils.timezone import utc
from django.contrib.admin.views.decorators import staff_member_required
from dateutil.relativedelta import relativedelta
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from pubmed.views import PubmedDict2TableDict, bucket
from backend.views import write_cron_job_log
from common.common import writeIntoDB, getFieldName, getCSVdata, run_mysql_script, rm, pt
from arxiv.models import *
from backend.script import *
from backend.models import CronJobLog
import urllib, urllib2, time, glob, os, re, boto,datetime
import numpy as np

def writeArxivCategoryData():
	#http://arxiv.org/help/api/user-manual#Architecture
	string="stat.AP	 Statistics - Applications,stat.CO	 Statistics - Computation,stat.ML	 Statistics - Machine Learning,stat.ME	 Statistics - Methodology,stat.TH	 Statistics - Theory,q-bio.BM	 Quantitative Biology - Biomolecules,q-bio.CB	 Quantitative Biology - Cell Behavior,q-bio.GN	 Quantitative Biology - Genomics,q-bio.MN	 Quantitative Biology - Molecular Networks,q-bio.NC	 Quantitative Biology - Neurons and Cognition,q-bio.OT	 Quantitative Biology - Other,q-bio.PE	 Quantitative Biology - Populations and Evolution,q-bio.QM	 Quantitative Biology - Quantitative Methods,q-bio.SC	 Quantitative Biology - Subcellular Processes,q-bio.TO	 Quantitative Biology - Tissues and Organs,cs.AR	 Computer Science - Architecture,cs.AI	 Computer Science - Artificial Intelligence,cs.CL	 Computer Science - Computation and Language,cs.CC	 Computer Science - Computational Complexity,cs.CE	 Computer Science - Computational Engineering; Finance; and Science,cs.CG	 Computer Science - Computational Geometry,cs.GT	 Computer Science - Computer Science and Game Theory,cs.CV	 Computer Science - Computer Vision and Pattern Recognition,cs.CY	 Computer Science - Computers and Society,cs.CR	 Computer Science - Cryptography and Security,cs.DS	 Computer Science - Data Structures and Algorithms,cs.DB	 Computer Science - Databases,cs.DL	 Computer Science - Digital Libraries,cs.DM	 Computer Science - Discrete Mathematics,cs.DC	 Computer Science - Distributed; Parallel; and Cluster Computing,cs.GL	 Computer Science - General Literature,cs.GR	 Computer Science - Graphics,cs.HC	 Computer Science - Human-Computer Interaction,cs.IR	 Computer Science - Information Retrieval,cs.IT	 Computer Science - Information Theory,cs.LG	 Computer Science - Learning,cs.LO	 Computer Science - Logic in Computer Science,cs.MS	 Computer Science - Mathematical Software,cs.MA	 Computer Science - Multiagent Systems,cs.MM	 Computer Science - Multimedia,cs.NI	 Computer Science - Networking and Internet Architecture,cs.NE	 Computer Science - Neural and Evolutionary Computing,cs.NA	 Computer Science - Numerical Analysis,cs.OS	 Computer Science - Operating Systems,cs.OH	 Computer Science - Other,cs.PF	 Computer Science - Performance,cs.PL	 Computer Science - Programming Languages,cs.RO	 Computer Science - Robotics,cs.SE	 Computer Science - Software Engineering,cs.SD	 Computer Science - Sound,cs.SC	 Computer Science - Symbolic Computation,nlin.AO	 Nonlinear Sciences - Adaptation and Self-Organizing Systems,nlin.CG	 Nonlinear Sciences - Cellular Automata and Lattice Gases,nlin.CD	 Nonlinear Sciences - Chaotic Dynamics,nlin.SI	 Nonlinear Sciences - Exactly Solvable and Integrable Systems,nlin.PS	 Nonlinear Sciences - Pattern Formation and Solitons,math.AG	 Mathematics - Algebraic Geometry,math.AT	 Mathematics - Algebraic Topology,math.AP	 Mathematics - Analysis of PDEs,math.CT	 Mathematics - Category Theory,math.CA	 Mathematics - Classical Analysis and ODEs,math.CO	 Mathematics - Combinatorics,math.AC	 Mathematics - Commutative Algebra,math.CV	 Mathematics - Complex Variables,math.DG	 Mathematics - Differential Geometry,math.DS	 Mathematics - Dynamical Systems,math.FA	 Mathematics - Functional Analysis,math.GM	 Mathematics - General Mathematics,math.GN	 Mathematics - General Topology,math.GT	 Mathematics - Geometric Topology,math.GR	 Mathematics - Group Theory,math.HO	 Mathematics - History and Overview,math.IT	 Mathematics - Information Theory,math.KT	 Mathematics - K-Theory and Homology,math.LO	 Mathematics - Logic,math.MP	 Mathematics - Mathematical Physics,math.MG	 Mathematics - Metric Geometry,math.NT	 Mathematics - Number Theory,math.NA	 Mathematics - Numerical Analysis,math.OA	 Mathematics - Operator Algebras,math.OC	 Mathematics - Optimization and Control,math.PR	 Mathematics - Probability,math.QA	 Mathematics - Quantum Algebra,math.RT	 Mathematics - Representation Theory,math.RA	 Mathematics - Rings and Algebras,math.SP	 Mathematics - Spectral Theory,math.ST	 Mathematics - Statistics,math.SG	 Mathematics - Symplectic Geometry,astro-ph	 Astrophysics,cond-mat.dis-nn	 Physics - Disordered Systems and Neural Networks,cond-mat.mes-hall	 Physics - Mesoscopic Systems and Quantum Hall Effect,cond-mat.mtrl-sci	 Physics - Materials Science,cond-mat.other	 Physics - Other,cond-mat.soft	 Physics - Soft Condensed Matter,cond-mat.stat-mech	 Physics - Statistical Mechanics,cond-mat.str-el	 Physics - Strongly Correlated Electrons,cond-mat.supr-con	 Physics - Superconductivity,gr-qc	 General Relativity and Quantum Cosmology,hep-ex	 High Energy Physics - Experiment,hep-lat	 High Energy Physics - Lattice,hep-ph	 High Energy Physics - Phenomenology,hep-th	 High Energy Physics - Theory,math-ph	 Mathematical Physics,nucl-ex	 Nuclear Experiment,nucl-th	 Nuclear Theory,physics.acc-ph	 Physics - Accelerator Physics,physics.ao-ph	 Physics - Atmospheric and Oceanic Physics,physics.atom-ph	 Physics - Atomic Physics,physics.atm-clus	 Physics - Atomic and Molecular Clusters,physics.bio-ph	 Physics - Biological Physics,physics.chem-ph	 Physics - Chemical Physics,physics.class-ph	 Physics - Classical Physics,physics.comp-ph	 Physics - Computational Physics,physics.data-an	 Physics - Data Analysis; Statistics and Probability,physics.flu-dyn	 Physics - Fluid Dynamics,physics.gen-ph	 Physics - General Physics,physics.geo-ph	 Physics - Geophysics,physics.hist-ph	 Physics - History of Physics,physics.ins-det	 Physics - Instrumentation and Detectors,physics.med-ph	 Physics - Medical Physics,physics.optics	 Physics - Optics,physics.ed-ph	 Physics - Physics Education,physics.soc-ph	 Physics - Physics and Society,physics.plasm-ph	 Physics - Plasma Physics,physics.pop-ph	 Physics - Popular Physics,physics.space-ph	 Physics - Space Physics,quant-ph	 Quantum Physics"
	cats=string.split(",")
	for cat in cats:
		dat = cat.split("\t")
		tmp = dat[1].split(" - ")
		data = {}
		data["Abbreviation"] = dat[0]
		data["Category"] = tmp[0]
		if len(tmp) > 1:
			data["Subcategory"] = tmp[1]
		writeIntoDB(Category, data)

def getKeydir(group):
	# return key directory for arive database
	keydir={}
	if group == "arxiv":
		keydir["link"] = '{http://www.w3.org/2005/Atom}id'
		keydir["title"] = "{http://www.w3.org/2005/Atom}title"
		keydir["PubDate"] = "{http://www.w3.org/2005/Atom}published"
		keydir["UpdatedDate"] = "{http://www.w3.org/2005/Atom}updated"
		keydir["summary"] = "{http://www.w3.org/2005/Atom}summary"
		keydir["Comment"] = "{http://arxiv.org/schemas/atom}comment"
		keydir["journal_ref"] = "{http://arxiv.org/schemas/atom}journal_ref"
		keydir["DOI"] = "{http://arxiv.org/schemas/atom}doi"
	elif group == "link":
		keydir["rel"] = "@rel"
		keydir["href"] = "@href"
		keydir["title"] = "@title"
	elif group == "author":
		keydir["name"] = "{http://www.w3.org/2005/Atom}name"
		keydir["affiliation"] = "{http://arxiv.org/schemas/atom}affiliation"
	return (keydir)
	
def parseArxivXMLFile(f,indb):
	tmp = f.split("/")
	tmp = tmp[len(tmp)-1]
	if settings.S3_DATA:
		pt("download %s... " % tmp)
		key = bucket.get_key(f)
		f = settings.BASE_DIR + "/tmp/" + tmp
		key.get_contents_to_filename(f)
		key.delete()
	count = 0
	out_arxiv = f.replace(".xml","_arxiv.csv")
	out_author = f.replace(".xml","_author.csv")
	tmp = f.split("/")
	tmp = tmp[len(tmp)-1].split("_")
	cat = Category.objects.get(Abbreviation=tmp[0])
	handle = open(f)
	tree = ElementTree.parse(handle)
	xmldict = etree_to_dict(tree.getroot())['{http://www.w3.org/2005/Atom}feed']
	if '{http://www.w3.org/2005/Atom}entry' in xmldict:
		# remove the file if exists
		rm(out_arxiv)
		rm(out_author)
		# open the file
		f_arxiv = open(out_arxiv,"w+")
		f_author = open(out_author,"w+")
		title_arxiv = getFieldName(Arxiv)
		title_author = getFieldName(Author)
		if Arxiv.objects.all().count() != 0:
			id_arxiv = Arxiv.objects.latest('id').id
		else:
			id_arxiv = 0
		if Author.objects.all().count() != 0:
			id_author = Author.objects.latest('id').id
		else:
			id_author = 0
		if str(type(xmldict['{http://www.w3.org/2005/Atom}entry'])).find('dict') != -1:
			tmp = xmldict['{http://www.w3.org/2005/Atom}entry']
			xmldict = {}
			xmldict['{http://www.w3.org/2005/Atom}entry'] = []
			xmldict['{http://www.w3.org/2005/Atom}entry'].append(tmp)
			tmp = ""
		for item in xmldict['{http://www.w3.org/2005/Atom}entry']:
			data = {}
			data = PubmedDict2TableDict(item,getKeydir("arxiv"))
			link = data["link"]
			if not link in indb:
				indb[link] = 1
				id_arxiv = id_arxiv + 1
				data["id"] = id_arxiv
				print "adding %s..." % link
				count = count +1
				#get the link value
				lk= item['{http://www.w3.org/2005/Atom}link']
				for l in lk:
					tmp={}
					tmp = PubmedDict2TableDict(l,getKeydir("link"))
					if "rel" in tmp:
						if tmp["rel"] == "alternate":
							data["LinkAbstract"] = tmp["href"]
					if "title" in tmp:
						if tmp["title"] == "pdf":
							data["LinkPDF"] = tmp["href"]
						if tmp["title"] == "doi":
							data["LinkDOI"] = tmp["href"]
				i = datetime.datetime.utcnow().replace(tzinfo=utc)
				data["DateToScireader"]=str(i.year) + "-" + str(i.month) + "-" + str(i.day)
				data["PubDate"]=datetime.datetime.strptime(data["PubDate"], "%Y-%m-%dT%H:%M:%SZ")
				data["PubDate"]=data["PubDate"].strftime('%Y-%m-%d')
				data["UpdatedDate"]=datetime.datetime.strptime(data["UpdatedDate"], "%Y-%m-%dT%H:%M:%SZ")
				data["UpdatedDate"]=data["UpdatedDate"].strftime('%Y-%m-%d')
				data["Category"] = cat.Category
				data["Subcategory"] = cat.Subcategory
				line = getCSVdata(title_arxiv, data)
				f_arxiv.write(line)
				#writeIntoDB(Arxiv ,data)
				# author data
				data={}
				authors = item['{http://www.w3.org/2005/Atom}author']
				if str(type(authors)).find('list') == -1:
					authors=[authors]
				for author in authors:
					data = PubmedDict2TableDict(author,getKeydir("author"))
					data["link"] = link
					id_author = id_author + 1
					data["id"] = id_author
					line = getCSVdata(title_author, data)
					f_author.write(line)
					#writeIntoDB(Author ,data)
			else:
				pt("skip %s" % link)
		f_arxiv.close()
		f_author.close()
	handle.close()
	# write comment
	#d = {}
	#d["app"] = "arxiv_parse"
	#d["description"] = "Parse arxiv xml files"
	#d["count"] = count
	#write_cron_job_log(d)
	
	if os.path.isfile(out_arxiv):
		run_mysql_script("LOAD DATA LOCAL INFILE \'" + out_arxiv + "\' INTO TABLE arxiv_arxiv FIELDS TERMINATED BY '\t' ESCAPED BY ''")
		rm(out_arxiv)
	if os.path.isfile(out_author):
		run_mysql_script("LOAD DATA LOCAL INFILE \'" + out_author + "\' INTO TABLE arxiv_author FIELDS TERMINATED BY '\t' ESCAPED BY ''")
		rm(out_author)
	rm(f)
	return indb
	
def parseArxivXML():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "arxiv_parse"
	if settings.S3_DATA:
		files = []
		for key in bucket.list():
			if key.name.find("arxiv") != -1:
				if key.name.find(".xml") != -1:
					files.append(key.name)
	else:
		files = glob.glob(settings.BASE_DIR + "/arxiv/download/*.xml")
	# load db into memery
	pt("Loading the recorded data...")
	td = datetime.datetime.now()
	dt1m = td - relativedelta(months = 1)
	records = Arxiv.objects.filter(DateToScireader__gte=dt1m)
	indb = {}
	for r in records:
		indb[r.link] = 1
	for f in files:
		indb = parseArxivXMLFile(f,indb)
	updateArxivSummaryTable()
	dlog["description"] = "Parsed arxiv papers and saved them into database"
	write_cron_job_log(dlog)

def downloadArxivXML(cat, total):
	total=int(total)
	batch_size = 500
	if batch_size > total:
		batch_size = total
	for start in range(0,total,batch_size):
		url = "http://export.arxiv.org/api/query?search_query=cat:"+cat.Abbreviation+"&start="+str(start)+"&max_results="+str(batch_size)+"&sortBy=lastUpdatedDate&sortOrder=descending"
		print "url: %s" % url
		
		fetch_handle = urllib2.urlopen(url)
		filename = "arxiv/download/" + cat.Abbreviation + "_" + str(start) + ".xml"
		if settings.S3_DATA:
			#write into s3
			k = Key(bucket)
			k.key = filename
			s = fetch_handle.read()
			k.set_contents_from_string(s)
		else:
			#write into local
			f = open(settings.BASE_DIR + "/" + filename,'w+')
			f.write(fetch_handle.read())
			f.close()
		time.sleep(3)

def checkArxivUpdates():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "arxiv_download"
	# update the category talbe
	if Category.objects.all().count() == 0:
		writeArxivCategoryData()
	# start checking
	count = 0;
	cats = Category.objects.all()
	#cats = Category.objects.filter(Abbreviation="cs.CE")
	for cat in cats:
		pt(cat.Abbreviation)
		url = "http://export.arxiv.org/api/query?search_query=cat:"+cat.Abbreviation+"&start=0&max_results=1"
		tree = ElementTree.parse(urllib.urlopen(url))
		xmldict = etree_to_dict(tree.getroot())['{http://www.w3.org/2005/Atom}feed']
		updated = xmldict['{http://www.w3.org/2005/Atom}updated']
		totalResults = xmldict['{http://a9.com/-/spec/opensearch/1.1/}totalResults']
		if cat.totalResults != totalResults or cat.updated != updated:
			newcount = int(totalResults) - int(cat.totalResults) + 10
			cat.totalResults = totalResults
			cat.updated = updated
			cat.save()
			downloadArxivXML(cat,newcount)
			# write comment
			dlog["description"] = "<b>%s</b> arxiv papers were downloaded" % newcount
			dlog["count"] = newcount
			write_cron_job_log(dlog)
		else:
			pt("No updates for %s" % cat.Abbreviation)
		time.sleep(3)

def updateArxivSummaryTable():
	dlog = {}
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	dlog["app"] = "arxiv_summary"
	cats = Category.objects.all()
	for cat in cats:
		d={}
		d["count"] = Arxiv.objects.filter(Subcategory = cat.Subcategory).count()
		title = str(cat.Category)
		if cat.Subcategory:
			title = str(cat.Category)+" - "+str(cat.Subcategory)
		d["subject"] = title
		if Summary.objects.filter(subject = d["subject"]).count() > 0:
			Summary.objects.filter(subject = d["subject"]).update(**d)
		else:
			writeIntoDB(Summary,d)
	# write comment
	dlog["description"] = "Summarized arxiv papers by table"
	write_cron_job_log(dlog)

@staff_member_required
def summary(request,view):
	data={}
	if view == "lr":
		data["tlr_sb"] = "arxiv/sidebar.html"
		data["tlr_content"] = "arxiv/summary.html"
		template = 'base_lr.html'
	else:
		template = "arxiv/summary.html"
	data['summary']=Summary.objects.all().order_by("subject")
	return render_to_response(template, data, context_instance=RequestContext(request))

@staff_member_required
def arxiv_cronjob_log(request):
    data = {}
    jobs = ["arxiv_download",
            "arxiv_parse",
            "arxiv_summary"
			]
    for j in jobs:
        data[j] = CronJobLog.objects.filter(app=j).order_by("-dtstamp")[:10]
    return render_to_response('pubmed/log.html', data, context_instance=RequestContext(request))