from django.shortcuts import render_to_response
from django.template import RequestContext, Template, Context
from django.core.urlresolvers import reverse
from django.core.mail import send_mail,EmailMultiAlternatives
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect
from django.forms.models import model_to_dict
from django.utils.timezone import utc
from django.template.loader import get_template
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.sites.models import Site
from django.conf import settings
from common.common import eid2id, writeIntoDB, writeIntoDBBulk, s2es, es2s
from emails.forms import *
from emails.models import *

import datetime,re

def cleanemail(email):
	if email.find("(") != -1:
		tmp = email.split("(")
		tmp = tmp[1].split(")")
		email = tmp[0]
	if email.find("<") != -1:
		tmp = email.split("<")
		tmp = tmp[1].split(">")
		email = tmp[0]
	email = re.sub("/\s/g",'',email)
	return email

def send_emails(data):
	# userid, user object, optional
	# templatename, string
	# tempalte, object, used for sending log
	# from_email, string
	# to_email, array
	# duplicated_check, true=do not send same email tempalte
	# tag, tag for duplicate check
	# other parameter that used for render the tempaltes
	
	if not "template" in data:
		data["template"] = Templates.objects.get(name=data["templatename"])
		print "Template is " + str(data["template"])
		print dir(data["template"])
		print "All templates"
		for template in Templates.objects.all():
		    print "Template name: " + template.name
		    
	if not "from_email" in data:
		data["from_email"] = settings.DEFAULT_FROM_EMAIL
	if User.objects.filter(email=data["from_email"]).count() > 0:
		data["from_id"] = User.objects.get(email=data["from_email"])
	# updated the history
	h = {}
	h["dtstamp"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	tmp = ["userid","template","from_email","from_id","tag"]
	for t in tmp:
		if t in data:
			h[t] = data[t]
	h["from_email"] = cleanemail(h["from_email"])
	# remove emails if already send out and duplicated_check = true
	elist = []
	for e in data["to_email"]:
		h["to_email"] = cleanemail(e)
		# not send the emails if duplicated
		do = False
		if data["duplicated_check"] == True:
			if "tag" in data:
				if History.objects.filter(template_id=data["template"].id,to_email=h["to_email"],tag=data["tag"]).count() == 0:
					do = True
			else:
				if History.objects.filter(template_id=data["template"].id,to_email=h["to_email"]).count() == 0:
					do = True
		else:
			if Unsubscribe_List.objects.filter(template_id=data["template"].id,email = h["to_email"]).count() == 0:
				do = True
		if do:
			if User.objects.filter(email=h["to_email"]).count() > 0:
				h["to_id"] = User.objects.get(email=h["to_email"])
			elist.append(h["to_email"])
			writeIntoDB(History,h)
	# prepare the emails
	data["subject"] = Template(data["template"].subject).render(Context(data))
	# subscribe link
	if len(elist) == 1:
		if data["template"].unsubscribe:
			st = Site.objects.get_current()
			data["unsubscribe"] = st.domain + reverse("emails_unsubscribe",args=[s2es(elist[0])])
	data["msg"] = Template(data["template"].template).render(Context(data))
	data["site"] = Site.objects.get_current()
	t = get_template('emails/body.html').render(Context(data))
	if len(elist) > 0:
		msg = EmailMultiAlternatives(data["subject"], t, data["from_email"], elist)
		msg.attach_alternative(t, "text/html")
		msg.send()
		
def send_emails_test():
	d = {}
	d["templatename"] = "test"
	d["to_email"] = ["prd@stanford.edu",'natalie.telis@gmail.com']
	d["duplicated_check"] = False
	send_emails(d)

def schema_emails(request,data):
	# view, tlr/lr, full display or lr only, default to tlr
	if not "view" in data:
		data["view"] = "tlr"
	template = 'base_lr.html'
	if data["view"] == "tlr":
		if not "nav_top" in data:
			data["nav_top"] = "emails"
		data["tlr_js_bottom"] = "backend/js_bottom.html"
		template = 'base_tlr.html'
	if not "tlr_content" in data:
		data["tlr_content"] = 'base_form.html'
	if "template" in data:
		template = data["template"]
	return render_to_response(template,data,context_instance=RequestContext(request))

@staff_member_required
def emails_home(request,view):
	data = {}
	data["view"] = view
	data["tlr_content"] = "emails/home.html"
	return schema_emails(request,data)

@staff_member_required
def emails_templates(request):
	data = {}
	data["templates"] = Templates.objects.all()
	return render_to_response('emails/templates_list.html', data, context_instance=RequestContext(request))

@staff_member_required
def emails_template_edit(request,etid):
	tid = eid2id(etid)
	data = {}
	if int(tid) != 0:
		data["template"] = Templates.objects.get(id=tid)
		data["title"] = "Edit " + data["template"].name
	else:
		data["title"] = "Add a email tempalte"
	data["form_ajax"] = "Dajaxice.emails.SubmitEmailTemplateForm(SubmittedForm,{'form':$('#id-form').serialize(true),'etid':'"+etid+"'})"
	if int(tid) != 0:
		arg = model_to_dict(data["template"], fields=[], exclude=[])
		if data["template"].unsubscribe:
			arg["unsubscribe"] = "Yes"
		else:
			arg["unsubscribe"] = "No"
		data["form"] =EmailTemplateForm(request.user,arg)
	else:
		data["form"] =EmailTemplateForm(request.user)
	return render_to_response('modal.html', data, context_instance=RequestContext(request))

@staff_member_required
def emails_template_view(request,etid):
	tid = eid2id(etid)
	data = {}
	data["template"] = Templates.objects.get(id=tid)
	data["msg"] = data["template"].template
	if Templates.objects.filter(name="header").count() > 0:
		data["header"] = Templates.objects.get(name="header").template
	if Templates.objects.filter(name="footer").count() > 0:
		data["footer"] = Templates.objects.get(name="footer").template
	return render_to_response('emails/template_view.html', data, context_instance=RequestContext(request))

@staff_member_required
def emails_send_collectinfo(request,etid):
	tid = eid2id(etid)
	data = {}
	data["t"] = Templates.objects.get(id=tid)
	data["title"] = "Sending emails"
	data["form_action"] = reverse("emails_send_collectinfo",args=[etid])
	if request.method == 'POST':
		data["form"] = EmailSendForm(request.user,data["t"],request.POST)
		if data["form"].is_valid():
			data["form"].save()
			return HttpResponseRedirect(reverse('emails_home',args=['tlr']))
		else:
			data["nav"] = "backend"
			data["tlr_content"] = 'emails/send_collectinfo.html'
			return schema_emails(request,data)
	else:
		data["form"] =EmailSendForm(request.user,data["t"])
	return render_to_response('emails/send_collectinfo.html', data, context_instance=RequestContext(request))

@staff_member_required
def emails_history(request):
	data = {}
	data["history"] = History.objects.all().order_by("-dtstamp")[:20]
	return render_to_response('emails/history.html', data, context_instance=RequestContext(request))

def emails_unsubscribe(request,eemail):
	email = es2s(eemail)
	data = {}
	data["view"] = "tlr"
	data["tlr_content"] = "emails/unsubscribe.html"
	data["email"] = email
	data["templates"] = Templates.objects.filter(unsubscribe=True)
	return schema_emails(request,data)

@staff_member_required
def emails_unsubscribe_list(request):
	data = {}
	data["unsubscribes"] = Unsubscribe_List.objects.all().order_by("-dtstamp")[:20]
	return render_to_response('emails/unsubscribe_list.html', data, context_instance=RequestContext(request))

