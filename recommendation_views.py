from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.utils.timezone import utc
from dateutil.relativedelta import relativedelta
from common.common import eid2id, id2eid, pt, rm, run_mysql_script, writeIntoDBBulk
from recommendations.models import *
from recommendations.forms import *
from pubmed.views import upload2s3
from library.models import Libraries, Library_Papers
from library.views import schema_paper_view,schema_paper_load, check_liked_library
from backend.views import write_cron_job_log
from backend.models import CronJobLog
from emails.views import send_emails
from accounts.views import add_visiting_record
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto, datetime
s3 = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY,host="s3-us-west-1.amazonaws.com")
bucket = s3.get_bucket('scireader-data')

def immediate_recs(request):
	newbee = False
	if request.user.is_authenticated():
		if Recommendations.objects.filter(userid=request.user.id).count() == 0:
			newbee = True
			from rec import immediateRecs
			# get topic ids
			ids = []
			for t in request.user.mytopics_set.all():
				ids.append(int(t.subcatid.id))
			if len(ids) > 0:
				pmids = [i.pmid for i in immediateRecs.get_first_recs(ids,settings.BASE_DIR+"/rec/current/Current_basic_dataset_with_IF_and_LDA.csv",None,None,use=[1,0,0])]
				rlist = []
				for p in pmids:
					d = {}
					d["userid"] = request.user
					d["dbname"] = "pubmed"
					d["dbid"] = p
					d["lid"] = 0
					rlist.append(d)
				writeIntoDBBulk(Recommendations,rlist)
	return newbee

def home(request):
	add_visiting_record(request)
	check_liked_library(request)
	data = {}
	data["newbee"]=immediate_recs(request)
	if request.user.is_authenticated():
		data["tlr_top"] = "recommendations/top.html"
	data["tlr_content"] = "recommendations/home.html"
	data["tlr_js_bottom"] = "recommendations/js_bottom.html"
	data["tlr_js_top"] = "recommendations/js_top.html"
	data["nav"] = "recommendations"
	data["title"] = "My recommendations"
	data["lid"] = 0
	data["elid"] = id2eid(0)
	return render_to_response('base_tlr.html',data, context_instance=RequestContext(request))

@login_required
def recom_view(request,elid):
	add_visiting_record(request)
	# library: lid=lid, userid=userid
	# personal: lid=0, userid=userid
	# liked: lid=-1,userid=userid
	# twitter: lid=-2,userid=userid
	from library.templatetags.userid_2_LibraryCarousel import library
	lid = eid2id(elid)
	data = {}
	data["load"] = reverse("recom_load",args=[elid,'more',1])
	data["paper_buttons"] = "recommendations/paper_twinview_page_buttons.html"
	data["lid"] = lid
	data["total"] = Recommendations.objects.filter(userid=request.user.id,lid=lid).count()
	# send out email if empty
	if data["total"] == 0:
		if lid != 0:
			d = {}
			d["templatename"] = "recommendations_empty_folder"
			d["to_email"] = ["ntelis@stanford.edu","prd@stanford.edu","pritch@stanford.edu"]
			d["duplicated_check"] = False
			d["user"] = request.user
			d["library"] = Libraries.objects.get(id=lid)
			send_emails(d)
	if int(lid) > 0:
		data["lib"] = Libraries.objects.get(id=lid)
	elif int(lid) == -1:
		data["lib"] = library(-1,request.user.id,"Liked Papers",None,None)
	data["edbid"] = elid
	return schema_paper_view(request, data)

@login_required
def recom_load(request, elid, batch, page):
	add_visiting_record(request)
	lid = eid2id(elid)
	page = int(page)
	data = {}
	data["edbid"] = elid
	data["page"] = page
	data["batch"] = batch
	data["url"] = reverse("recom_load",args=[elid,"BATCH",0])
	data["papers"] = Recommendations.objects.filter(userid=request.user.id,lid=lid)
	return schema_paper_load(request,data)

def recom_import_s3():
	dlog = {}
	dlog["app"] = "recom_import_s3"
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	# get the files
	pt("Checking the new recommendation file...")
	tags = list(bucket.list(prefix='recommendations/new/'))
	libcount = 0
	if len(tags) >= 2:
		n = ""
		for key in bucket.list(prefix='recommendations/new/'):
			if key.name.find(".txt") != -1:
				n = key.name
				pt("Downloading %s from s3..." % key.name)
				f = "%s/tmp/recommendations.txt" % settings.BASE_DIR
				rm(f)
				key.get_contents_to_filename(f)
				pt("Parsing the data...")
				dblist = {} # to store all data, child = dbarray
				with open(f) as fh:
					uq = {} # unique check
					for line in fh:
						line = line.replace("\n","")
						cols = line.split("\t")
						k = "%s_%s" % (str(cols[1]),str(cols[2]))
						if not k in dblist:
							dblist[k] = []
						d = {}
						if cols[1] != "NULL":
							d["userid_id"] = cols[1]
						d["lid"] = cols[2]
						d["dbname"] = cols[3]
						d["dbid"] = cols[4]
						if len(cols) > 5:
							if cols[5] != "NULL":
								d["subcat_id"] = cols[5]
						q = "%s_%s" % (d["lid"],d["dbid"])
						if not q in uq:
							dblist[k].append(d)
							uq[q] = 1;
				pt("Loading into database...")
				for k in dblist:
					libcount = libcount + 1
					ids = k.split("_")
					pt("Loading user (%s), library (%s) ... " % (str(ids[0]),str(ids[1])))
					if ids[0] == "NULL":
						Recommendations.objects.filter(userid_id__isnull=True,lid=ids[1]).delete()
					else:
						Recommendations.objects.filter(userid_id=ids[0],lid=ids[1]).delete()
					writeIntoDBBulk(Recommendations,dblist[k])
					# update the library status
					if int(ids[1]) > 0:
						d = {}
						d["status"] = None
						Libraries.objects.filter(id=ids[1]).update(**d)
						# send out the notification emails
						"""d = {}
						d["templatename"] = "library_import_done"
						d["library"] = Libraries.objects.get(id=ids[1])
						d["to_email"] = [d["library"].userid.email]
						d["duplicated_check"] = True
						d["tag"] = "library_%i" % d["library"]
						d["user"] = d["library"].userid
						d["count"] = Library_Papers.objects.filter(libid_id=d["library"].id).count()
						send_emails(d)"""
				pt("Moving %s to loaded..." % key.name)
				newkey = key.name.replace("new","loaded")
				upload2s3(f,newkey)
				pt("Deleting %s from s3..." % key.name)
				key.delete()
				rm(f)
		# write comment
		dlog["description"] = "Import recommendation files for <b>%i</b> libraries" % libcount
		dlog["count"] = libcount
		write_cron_job_log(dlog)
	else:
		pt("Nothing to update.")
		
def recom_error_report():
	td = datetime.datetime.utcnow().replace(tzinfo=utc)
	dt1d = td - relativedelta(days = 1)
	# report user overall
	dlog = {}
	dlog["app"] = "recom_error_report"
	dlog["dtstart"] = datetime.datetime.utcnow().replace(tzinfo=utc)
	from django.contrib.auth.models import User
	from feeds.models import MyTopics
	users = User.objects.filter(date_joined__lte=dt1d)
	added = {}
	added["p"] = 0
	added["t"] = 0
	good = True
	totolusers = 0
	for u in users:
		if MyTopics.objects.filter(userid_id=u.id).count() > 0 or Library_Papers.objects.filter(userid_id=u.id).count() > 0:
			totolusers = totolusers + 1
			if Recommendations.objects.filter(userid_id=u.id,lid = 0).count() > 0:
				added["p"] = added["p"] + 1
			if Recommendations.objects.filter(userid_id=u.id,lid = -2).count() > 0:
				added["t"] = added["t"] + 1
	dlog["description"] = "<b>%i</b> of <b>%i</b> users have personal recommendaitons" % (added["p"], totolusers)
	dlog["description"] = dlog["description"] + "<br><b>%i</b> of <b>%i</b> users have twitter recommendaitons" % (added["t"], totolusers)
	if Recommendations.objects.filter(userid_id__isnull=True,lid = -2).count() > 0:
		dlog["description"] = dlog["description"] + "<br>Found twitter recommendations for null users."
	else:
		dlog["description"] = dlog["description"] + "<br>NOT Found twitter recommendations for null users."
		good = False
	if added["p"] < totolusers:
		good = False
	if added["t"] < totolusers:
		good = False
	# libraries
	added["l"] = 0
	totallibs = 0
	libs = Libraries.objects.filter(DateCreated__lte=dt1d)
	for lib in libs:
		if Library_Papers.objects.filter(libid_id=lib.id).count() > 0:
			totallibs = totallibs + 1
			if Recommendations.objects.filter(lid = lib.id).count() > 0:
				added["l"] = added["l"] + 1
	dlog["description"] = dlog["description"] + "<br><b>%i</b> of <b>%i</b> libraries have recommendations" % (added["l"], totallibs)
	if added["l"] < totallibs:
		good = False
	pt(dlog["description"])
	if good == False:
		dlog["success"] = False
	write_cron_job_log(dlog)
		
def recom_onhome(request):
	add_visiting_record(request)
	data={}
	data["total"] = Recommendations.objects.filter(userid=request.user.id,lid=0).count()
	data["edbid"] = id2eid(request.user.id)
	data["app"] = "recom_onhome"
	data["data_target"] = "#recom_onhome"
	return render_to_response('recommendations/onhome.html',data,context_instance=RequestContext(request))
	
def recom_onhome_load(request,edbid,batch,page):
	add_visiting_record(request)
	data={}
	data["page"] = page
	data["perpage"] = 20
	data["total"] = Recommendations.objects.filter(userid=request.user.id,lid=0).count()
	data["start"] = (int(page)-1)*data["perpage"] +1
	if batch == "more":
		data["end"] = data["start"] + data["perpage"] -1
	else:
		data["end"] = data["total"]
	data["more"] = False
	if data["end"] < data["total"]:
		data["more"] = reverse("recom_onhome_load",args=[edbid,"more",int(data["page"]) + 1])
		data["all"] = reverse("recom_onhome_load",args=[edbid,"all",int(data["page"]) + 1])
	else:
		data["end"] = data["total"]
	data["papers"] = Recommendations.objects.filter(userid=request.user.id,lid=0)[data["start"]-1:data["end"]]
	data["edbid"] = edbid
	data["app"] = "recom_onhome"
	return render_to_response('recommendations/onhome_list.html',data,context_instance=RequestContext(request))

@staff_member_required
def recom_backend(request):
	data = {}
	data["tlr_sb"] = "recommendations/backend_sidebar.html"
	return render_to_response('base_lr.html',data,context_instance=RequestContext(request))

@staff_member_required
def recom_backend_import(request):
	data={}
	data["title"] = "Import Recommendations"
	data["form_action"] = reverse("recom_backend_import")
	data["form_enctype"] = True
	data["form_wide"] = True
	if request.method == 'POST':
		data["form"] = RecommendationImportForm(request.user,request.POST, request.FILES)
		if data["form"].is_valid():
			data["form"].save({'file':request.FILES['uploadfile']})
			data["message"] = "Successfully imported!"
			data["tlr_content"] = "recommendations/backend_import.html"
			data["tlr_sb"] = "recommendations/backend_sidebar.html"
			data["tlr_top"] = "backend/top.html"
			return render_to_response("base_tlr.html", data, context_instance=RequestContext(request))
		else:
			data["tlr_sb"] = "recommendations/backend_sidebar.html"
			data["tlr_top"] = "backend/top.html"
			data["tlr_content"] = "recommendations/backend_import.html"
			return render_to_response("base_tlr.html", data, context_instance=RequestContext(request))
	else:
		data["form"] = RecommendationImportForm(request.user)
	return render_to_response("recommendations/backend_import.html", data, context_instance=RequestContext(request))

def recom_email_weekly():
	# send out the emails
	# collect the users
	from django.contrib.auth.models import User
	#users = []
	#emails = ["pritch@stanford.edu","priyamvadadesai@gmail.com","natalie.telis@gmail.com","dcal@stanford.edu","kon10004@gmail.com","pmelsted@gmail.com","anand.bhaskar@gmail.com","r.hunte@med.miami.edu","j.roebber@med.miami.edu","g.edwards3@med.miami.edu","jkpickrell@nygenome.org","eglassbe@stanford.edu","vinoyvjn@gmail.com","rxghosh2@texaschildrens.org","lmf@medicina.ulisboa.pt","danbolnick@austin.utexas.edu","garlandm@stanford.edu","mark.larios@gmail.com","davisf@janelia.hhmi.org","jorgensen@biology.utah.edu","sam.lambert@mail.utoronto.ca","zolotarov@gmail.com","rossibarra@ucdavis.edu","brantp@gmail.com","peter.combs@berkeley.edu","yukikomy@umich.edu","greenro6@uw.edu","davidgo@stanford.edu","razkhan@ucdavis.edu","liz.ingsimmons@gmail.com","nick.banovich@gmail.com","audreyqyfu@gmail.com","bg200@cam.ac.uk","dhinds@23andme.com","sebastian.pott@gmail.com","jfdegner@gmail.com","ireneg@uchicago.edu","rajanil@stanford.edu","dmitristanford@gmail.com","david.plachetzki@unh.edu","narayanan.r@gmail.com","ajspakow@stanford.edu","cxh@stanford.edu","amycole@rcsi.ie","zhenguo@gmail.com"]
	#for e in emails:
	#	print e
	#	users.append(User.objects.get(email=e))
	# send out emails
	count = 0
	for u in User.objects.all():
		if Recommendations.objects.filter(userid_id=u.id,lid=-3).count() > 0:
			d = {}
			d["templatename"] = "recommendations_weekly_update"
			d["to_email"] = [u.email]
			d["duplicated_check"] = False
			d["to_id"] = u
			d["papers"] = Recommendations.objects.filter(userid_id=u.id,lid=-3)[:10]
			d["tag"] = "11"
			send_emails(d)
			Recommendations.objects.filter(userid_id=u.id,lid=-3).delete()
			count  = count + 1
	pt("Send out %s emails" % count,'blue')


def recom_email_weekly_test():
	# send out the emails
	# collect the users
	from django.contrib.auth.models import User
	users = []
	emails = ["natalie.telis@gmail.com","priyamvadadesai@gmail.com","pritch@stanford.edu"]
	for e in emails:
		users.append(User.objects.get(email=e))
	# send out emails
	for u in users:	
		d = {}
		d["templatename"] = "recommendations_weekly_update"
		d["to_email"] = [u.email]
		d["duplicated_check"] = False
		d["to_id"] = u
		d["papers"] = Recommendations.objects.filter(userid_id=u.id,lid=-3)[:10]
		d["tag"] = "11"
		send_emails(d)
		Recommendations.objects.filter(userid_id=u.id,lid=-3).delete()