import os, re, base64, random, random

def writeIntoDB(db, data):
	# DB, database object
	# data, hash data with keys
	dt=db.objects.create(**data)
	dt.save()
	return dt

def writeIntoDBBulk(db, data):
	# DB, database object
	# data, array of hash data with keys
	db.objects.bulk_create([db(**d) for d in data])
	
def writeIntoDBSingleInsert(db,data):
	# DB, database object
	# data, array of hash data with keys
    #print "data:",data[0]
    print "datatype",type(data) 
    count=0
    for d in data:
    	try:
    	    #print "debug3"
    	    #print d
    	    db.objects.bulk_create([db(**d)])
    	except:
    	    print "could not write to db"   	
            count+=1
    print "Num of pmids that could not be written to database:", count        
             
def getFieldName(db):
	# return array of field title for a database
	title = []
	for f in db._meta.fields:
		title.append(str(f.name))
	return title

def getCSVdata(title, data):
	# return csv line from an array of title and a dic
	line = str(data["id"])
	for t in title:
		if t != "id":
			if t in data:
				line = line + "\t" + str(data[t])
			else:
				line = line + "\tNULL"
	line = line + "\n"
	return line

def readPostValue(p,title):
	# extract the post value
	# remove the key if empty
	data={}
	for t in title:
		data[t]=p.get(t,"")
		if data[t] == "":
			data.pop(t,None)
	return data

def handle_uploaded_file(f,filename):
	# update a file
	with open(filename, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)

def check_templatetags(name,number,token):
	# templates
	from django.template import TemplateSyntaxError
	bits = token.contents.split()
	if number == 1:
		if bits[1] != 'as':
			raise TemplateSyntaxError, "2th argument to "+name+" tag must be 'as'"
	else:
		total = number + 3
		p_as = total - 2
		if len(bits) != total:
			raise TemplateSyntaxError, name + " tag takes exactly "+ str(total) +" arguments"
		if bits[1] != 'for':
			raise TemplateSyntaxError, "2rd argument to "+name+" tag must be 'for'"
		if bits[p_as] != 'as':
			raise TemplateSyntaxError, str(p_as + 1) + "3rd argument to "+name+" tag must be 'as'"
	if number == 2:
		return [bits[2], bits[4]]
	elif number == 3:
		return [bits[2], bits[3], bits[5]]
	elif number == 4:
		return [bits[2], bits[3], bits[4], bits[6]]
	elif number == 5:
		return [bits[2], bits[3], bits[4], bits[5], bits[7]]
	elif number == 1:
		return [bits[2]]
	else:
		raise TemplateSyntaxError, "The template tag accepts at most 5 arguments"
		
def run_mysql_script(query):
	# run mysql script
	from django.db import connection
	from warnings import filterwarnings
	filterwarnings('ignore')
	cursor = connection.cursor()
	#print query
	cursor.execute(query)
        connection.close()
    
    
# enctypt for numbers
def id2eid(id):
	chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	salt = ""
	salt2 = ""
	for k in range(1,5):
		salt = salt + random.choice(chars)
		salt2 = salt2 + random.choice(chars)
	eid = hex(int(id))
	return "%s%s%s" % (salt,eid,salt2)

def id2eid_static(id):
	chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	salt = ""
	salt2 = ""
	for k in range(1,5):
		salt = str(salt) + list(chars)[(id+k*3)-int((id+k*3)/62)*62]
		salt2 = str(salt2) + list(chars)[(id+k*5)-int((id+k*5)/62)*62]
	eid = hex(int(id))
	return "%s%s%s" % (salt,eid,salt2)

def eid2id(eid):
	eid = eid[4:-4]
	try:
		id = int(eid,16)
	except:
		id = False
	return id

# enctypt for string
def s2es(id):
	chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	salt = ""
	salt2 = ""
	for k in range(1,5):
		salt = salt + random.choice(chars)
		salt2 = salt2 + random.choice(chars)
	return base64.b64encode("%s%s%s" % (salt,id,salt2))

def s2es_static(id):
	chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	salt = ""
	salt2 = ""
	for k in range(1,5):
		salt = str(salt) + list(chars)[(len(id)+k*7)-int((len(id)+k*7)/62)*62]
		salt2 = str(salt2) + list(chars)[(len(id)+k*9)-int((len(id)+k*9)/62)*62]
	return base64.b64encode("%s%s%s" % (salt,id,salt2))

def es2s(es):
	try:
		s = base64.b64decode(es)
		s = s[4:-4]
	except:
		s = False
	return s

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

def get_query(query_string, search_fields):
	''' Returns a query, that is a combination of Q objects. That combination
		aims to search keywords within a model by testing the given search fields.
	'''
	from django.db.models import Q
	query = None # Query to search for every search term        
	terms = normalize_query(query_string)
	for term in terms:
		or_query = None # Query to search for a given term in each field
		for field_name in search_fields:
			q = Q(**{"%s__icontains" % field_name: term})
			if or_query is None:
				or_query = q
			else:
				or_query = or_query | q
		if query is None:
			query = or_query
		else:
			query = query & or_query
	return query

def mem():
	# return memory usage percentage
	# available only on linux
	import psutil
	mem = psutil.virtual_memory()
	return "%s%%" % mem.percent

def pt(content,*args, **kwargs):
	# content, print content, as string
	# color, true or false
	from time import strftime
	if len(args)>0:
		if str(args[0]) == "green":
			color = '\033[92m'
		elif str(args[0]) == "red":
			color = '\033[95m'
		elif str(args[0]) == "blue":
			color = '\033[94m'
		elif str(args[0]) == "yellow":
			color = '\033[93m'
		else:
			color = '\033[92m'
		print "%s[%s Mem: %s] %s%s" % (color, strftime("%Y-%m-%d %H:%M:%S"),mem(), content, '\033[0m')
	else:
		print "[%s Mem: %s] %s" % (strftime("%Y-%m-%d %H:%M:%S"),mem(),content)
		
def rm(filename):
	if os.path.isfile(filename):
		os.remove(filename)
		
# to delete an item from ajax
def ajax_rm_item(db,eid,prefix,*args, **kwargs):
	# db, the database
	# eid, the encoded id of database
	# prefix, the prefix of a element
	# args = dict to return an dictionary
	import json
	id = eid2id(eid)
	db.objects.filter(id=id).delete()
	data = {}
	data["id"] = eid
	data["prefix"] = prefix
	if len(args)>0:
		if args[0] == 'dict':
			return data
	else:
		return json.dumps(data)
	
def random_color():
	# generate a random color
	import random
	color = ["235FA1","4e4e4e","73490b","731d0b","730b38","620b73","0a6b5c","0a6b31","e46f0a","e4450a","920c68","340c9b","0c2c9b","0c5c92","0c926c","75920c","928d0c","924f0c"]
	return color[random.randint(0,len(color))-1]

def upload2s3(bucket,source_path,target):
    import math
    from filechunkio import FileChunkIO
    pt("Uploading %s to S3://%s..." % (source_path,target),False)
    source_size = os.stat(source_path).st_size
    mp = bucket.initiate_multipart_upload(target)
    chunk_size = 52428800
    chunk_count = int(math.ceil(source_size / chunk_size))
    for i in range(chunk_count + 1):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(source_path, 'r', offset=offset,bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()

def l2l(link):
	# link to link
	# input target:xxx-xx-xx
	# use SELF to link back
	# use none for nothing
	# if '--', indicated link of link
	if link == "none":
		return None
	else:
		from django.core.urlresolvers import reverse
		tmp = link.split(":")
		oldlink = link
		target = None
		if len(tmp) > 1: # target
			target = "#" + tmp[0]
			link = tmp[1]
			
		# separate by '--' first if exist, used for links of links
		if link.find('--') != -1:
			tmp = link.split("--")
		else:
			tmp = link.split("-")
			
		if len(tmp) == 1:
			if target:
				return target,reverse(tmp[0])
			else:
				return reverse(tmp[0])
		else:
			linkname = tmp[0]
			tmp.pop(0)
			if tmp[len(tmp) - 1] == "SELF":
				tmp.pop(len(tmp) - 1)
				tmp.append(oldlink)
			if target:
				return target,reverse(linkname,args=tmp)
			else:
				return reverse(linkname,args=tmp)
	